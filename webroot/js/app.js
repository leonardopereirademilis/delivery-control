var myApp = angular.module('myApp', []);

myApp.controller('dispatchsController', ['$scope', '$http', function ($scope, $http) {

    init();

    function init() {

        $scope.addDispatch = {
            company: 0,
            carriers: [{id: '0', name: 'Select a company first'}],
            carrier: 0,
            disableCarrier: true,
            addressShow: true,
            parte1Show: true,
            parte2Show: false,
            parte3Show: false,
            parte4Show: false
        }

        $scope.addDispatch.carrier = $scope.addDispatch.carriers[0];
    }

    $scope.companyChange = function () {
        $scope.addDispatch.disableCarrier = true;
        $scope.addDispatch.carriers[0] = {id: '0', name: 'Loading carriers...'};
        if ($scope.addDispatch.company != 0) {
            var request = $http({
                method: "get",
                url: "../carriers.json?company=" + $scope.addDispatch.company,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function successCallback(response) {
                $scope.addDispatch.carriers = new Array();
                $scope.addDispatch.carriers[0] = {id: '0', name: 'Select a carrier'};

                angular.forEach(response.data.carriers, function (carrier, id) {
                    $scope.addDispatch.carriers.push({id: id, name: carrier});
                });

                $scope.addDispatch.disableCarrier = false;

                $scope.addDispatch.carrier = $scope.addDispatch.carriers[0];

            }, function errorCallback(response) {
                $scope.addDispatch.carriers = new Array();
                $scope.addDispatch.carriers = [{id: '0', name: 'Select a company first'}];
                $scope.addDispatch.disableCarrier = true;

                $scope.addDispatch.parte4Show = false;
            });

        } else {
            $scope.addDispatch.carriers = new Array();
            $scope.addDispatch.disableCarrier = true;
            $scope.addDispatch.carriers = [{id: '0', name: 'Select a company first'}];

            $scope.addDispatch.parte4Show = false;
        }

        $scope.addDispatch.carrier = $scope.addDispatch.carriers[0];
    };

    $scope.userSelected = function () {
        var user_id = $scope.addDispatch.user_name.split('id: ').pop().toString().trim();
        if ($scope.addDispatch.user_name.trim() != "") {
            var request = $http({
                method: "get",
                url: "../addresses.json?user=" + user_id,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function successCallback(response) {
                if (response.data.addresses.length > 0) {
                    $scope.addDispatch.addresses = response.data.addresses;
                    $scope.addDispatch.addressShow = false;
                }else {
                    $scope.addDispatch.addresses = null;
                    $scope.addDispatch.addressShow = true;
                 }
            }, function errorCallback(response) {
                console.log(response);
                $scope.addDispatch.addresses = null;
                $scope.addDispatch.addressShow = true;
            });
        } else {
            console.log(response);
            $scope.addDispatch.addresses = null;
            $scope.addDispatch.addressShow = true;
        }

        $scope.addDispatch.parte2Show = true;
        $scope.addDispatch.parte3Show = false;
        $scope.addDispatch.parte4Show = false;
    };

    $scope.addDispatch.selectAddress = function(lat, lng, id) {
        var latlng_str = "{lat:" + lat + ", lng:" + lng + "}";

        angular.forEach($scope.addDispatch.addresses, function (address, i) {
            $("#selected-" + address.id).html('');
        });

        $("#destination").val(latlng_str);
        $("#lat").val(lat);
        $("#lng").val(lng);
        $("#selected-" + id).html('<span class="glyphicon glyphicon-ok"></span>');
        $("#address-id").val(id);

        $scope.addDispatch.parte3Show = true;
        $scope.addDispatch.parte4Show = false;
    };

    $scope.carrierChange = function () {
        if ($scope.addDispatch.carrier.id != 0) {
            $scope.addDispatch.parte4Show = true;
            $("#dispatchAddSubmit").prop( "disabled", false );
        }else{
            $scope.addDispatch.parte4Show = false;
            $("#dispatchAddSubmit").prop( "disabled", true );
        }
    }

}]);