<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrderStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrderStatusTable Test Case
 */
class OrderStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrderStatusTable
     */
    public $OrderStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.order_status',
        'app.orders',
        'app.users',
        'app.dispatchs',
        'app.carriers',
        'app.companies',
        'app.resources',
        'app.plans',
        'app.addresses',
        'app.addresses_users',
        'app.roles',
        'app.permissions',
        'app.permissions_roles',
        'app.roles_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrderStatus') ? [] : ['className' => 'App\Model\Table\OrderStatusTable'];
        $this->OrderStatus = TableRegistry::get('OrderStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
