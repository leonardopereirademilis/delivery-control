<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DispatchsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DispatchsTable Test Case
 */
class DispatchsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DispatchsTable
     */
    public $Dispatchs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dispatchs',
        'app.users',
        'app.plans',
        'app.resources',
        'app.carriers',
        'app.companies',
        'app.addresses',
        'app.addresses_users',
        'app.roles',
        'app.permissions',
        'app.permissions_roles',
        'app.roles_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Dispatchs') ? [] : ['className' => 'App\Model\Table\DispatchsTable'];
        $this->Dispatchs = TableRegistry::get('Dispatchs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Dispatchs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
