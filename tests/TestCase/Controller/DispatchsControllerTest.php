<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DispatchsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DispatchsController Test Case
 */
class DispatchsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dispatchs',
        'app.users',
        'app.plans',
        'app.resources',
        'app.carriers',
        'app.companies',
        'app.addresses',
        'app.addresses_users',
        'app.roles',
        'app.permissions',
        'app.permissions_roles',
        'app.roles_users'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
