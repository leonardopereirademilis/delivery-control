<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return !in_array($schema->columnType($field), ['binary', 'text']);
    });

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
    $fields = $fields->reject(function ($field) {
        return $field === 'lft' || $field === 'rght';
    });
}

if (!empty($indexColumns)) {
    $fields = $fields->take($indexColumns);
}

%>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('/Menu/menu'); ?>
</nav>
<div class="<%= $pluralVar %> index large-9 medium-8 columns content">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' <%= $pluralHumanName %>') ?></h3>
    <div class="table-responsive"><table class="table table-striped table-bordered">
        <thead>
            <tr>
<% foreach ($fields as $field): %>
                <th scope="col"><?= $this->Paginator->sort('<%= $field %>') ?></th>
<% endforeach; %>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($<%= $pluralVar %> as $<%= $singularVar %>): ?>
            <tr>
<%        foreach ($fields as $field) {
            $isKey = false;
            if (!empty($associations['BelongsTo'])) {
                foreach ($associations['BelongsTo'] as $alias => $details) {
                    if ($field === $details['foreignKey']) {
                        $isKey = true;
%>
                <td><?= $<%= $singularVar %>->has('<%= $details['property'] %>') ? $this->Html->link($<%= $singularVar %>-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $<%= $singularVar %>-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></td>
<%
                        break;
                    }
                }
            }
            if ($isKey !== true) {
                if (!in_array($schema->columnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
%>
                <td><?= h($<%= $singularVar %>-><%= $field %>) ?></td>
<%
                } else {
%>
                <td><?= $this->Number->format($<%= $singularVar %>-><%= $field %>) ?></td>
<%
                }
            }
        }

        $pk = '$' . $singularVar . '->' . $primaryKey[0];
%>
                <td class="actions">
                    <?php
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['<%= ucfirst($pluralVar) %>Controller']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['action' => 'view', <%= $pk %>]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['<%= ucfirst($pluralVar) %>Controller']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['action' => 'edit', <%= $pk %>]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['<%= ucfirst($pluralVar) %>Controller']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['action' => 'delete', <%= $pk %>], ['confirm' => __('Are you sure you want to delete # {0}?', <%= $pk %>)]);
                        }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table></div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
