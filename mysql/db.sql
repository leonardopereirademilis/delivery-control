-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.9-log - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela lpdemilis_db.addresses
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `street_number` varchar(80) NOT NULL,
  `route` varchar(255) NOT NULL,
  `sublocality_level_1` varchar(80) NOT NULL,
  `locality` varchar(80) NOT NULL,
  `administrative_area_level_2` varchar(80) NOT NULL,
  `administrative_area_level_1` varchar(80) NOT NULL,
  `country` varchar(80) NOT NULL,
  `postal_code` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.addresses: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` (`id`, `name`, `address`, `lat`, `lng`, `street_number`, `route`, `sublocality_level_1`, `locality`, `administrative_area_level_2`, `administrative_area_level_1`, `country`, `postal_code`) VALUES
	(1, 'Casa', 'R. Salvatina Feliciana dos Santos, 155 - Itacorubi Florianópolis - SC', -27.591558, -48.493229, '155', 'R. Salvatina Feliciana dos Santos', 'Itacorubi', 'Florianópolis', 'Florianópolis', 'Santa Catarina', 'Brasil', '88034'),
	(2, 'Casa', 'R. Salvatina Feliciana dos Santos, 155', -27.591606, -48.492672, '155', 'R. Salvatina Feliciana dos Santos', 'Itacorubi', 'Florianópolis', 'Florianópolis', 'Santa Catarina', 'Brasil', '88034'),
	(3, 'Comercial', 'Centro de Informática e Automação do Estado de Santa Catarina - R. Murilo Andriani, 327 - Santa Monica, Florianópolis - SC, 88034-902, Brasil', -27.587402, -48.499813, '327', 'Rua Murilo Andriani', 'Santa Monica', 'Florianópolis', 'Florianópolis', 'Santa Catarina', 'Brasil', '88034'),
	(4, 'Centro de Informática e Automação do Estado de Santa Catarina', 'Centro de Informática e Automação do Estado de Santa Catarina - R. Murilo Andriani, 327 - Santa Monica, Florianópolis - SC, 88034-902, Brasil', -27.587402, -48.499813, '327', 'Rua Murilo Andriani', 'Santa Monica', 'Florianópolis', 'Florianópolis', 'Santa Catarina', 'Brasil', '88034-902');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.addresses_users
DROP TABLE IF EXISTS `addresses_users`;
CREATE TABLE IF NOT EXISTS `addresses_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `address_id` (`address_id`),
  KEY `FK_addresses_users_users` (`user_id`),
  CONSTRAINT `FK_addresses_users_addresses` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`),
  CONSTRAINT `FK_addresses_users_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.addresses_users: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `addresses_users` DISABLE KEYS */;
INSERT INTO `addresses_users` (`id`, `address_id`, `user_id`) VALUES
	(1, 1, 2),
	(2, 2, 2),
	(3, 3, 2),
	(4, 4, 2);
/*!40000 ALTER TABLE `addresses_users` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.carriers
DROP TABLE IF EXISTS `carriers`;
CREATE TABLE IF NOT EXISTS `carriers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `base_position` varchar(100) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `status` enum('Online','Offline') NOT NULL DEFAULT 'Offline',
  `datemodified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `imei` varchar(50) NOT NULL,
  `resource_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `resource_id` (`resource_id`),
  KEY `FK_carriers_companies` (`company_id`),
  CONSTRAINT `FK_carriers_companies` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `FK_carriers_resources` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.carriers: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `carriers` DISABLE KEYS */;
INSERT INTO `carriers` (`id`, `name`, `base_position`, `position`, `status`, `datemodified`, `company_id`, `imei`, `resource_id`) VALUES
	(1, 'Entregador 1', '', '{lat: 37.421998333333335, lng: -122.08400000000002}', 'Online', '2017-05-18 01:15:26', 1, '000000000000000', 3),
	(2, 'Entregador 2', '', '{lat: -27.59093013737297, lng: -48.491973802156295}', 'Online', '2017-05-19 01:00:54', 1, '358151070586978', 4),
	(3, 'Entregador 3', '', '{lat: -27.59093013737297, lng: -48.491973802156295}', 'Online', '2017-05-19 01:00:54', 2, '358151070586978', 5),
	(4, 'Entregador 4', '', '{lat: -27.59093013737297, lng: -48.491973802156295}', 'Online', '2017-05-19 01:00:54', 5, '358151070586978', 10);
/*!40000 ALTER TABLE `carriers` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.companies
DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `position` varchar(100) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `resource_id` (`resource_id`),
  KEY `FK_companies_addresses` (`address_id`),
  CONSTRAINT `FK_companies_addresses` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`),
  CONSTRAINT `FK_companies_resources` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.companies: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` (`id`, `name`, `position`, `resource_id`, `address_id`) VALUES
	(1, 'Empresa 1', '{lat: -27.590114, lng: -48.543081}', 1, 3),
	(2, 'Empresa 2', '{lat: -27.589239, lng: -48.516946}', 2, 3),
	(4, 'Empresa 3', '{lat: -27.595365, lng: -48.517555}', 6, 3),
	(5, 'Empresa 4', '{lat: -27.587187, lng: -48.498172}', 9, 3);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.configs
DROP TABLE IF EXISTS `configs`;
CREATE TABLE IF NOT EXISTS `configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.configs: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `configs` DISABLE KEYS */;
INSERT INTO `configs` (`id`, `name`, `value`) VALUES
	(1, 'freecompanies', '1'),
	(2, 'freecarriers', '2');
/*!40000 ALTER TABLE `configs` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.dispatchs
DROP TABLE IF EXISTS `dispatchs`;
CREATE TABLE IF NOT EXISTS `dispatchs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `destination` varchar(100) NOT NULL,
  `carrier_id` int(11) NOT NULL,
  `timestart` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timeend` timestamp NULL DEFAULT NULL,
  `status` enum('Pending','Delivered','Canceled') NOT NULL DEFAULT 'Pending',
  `token` varchar(50) NOT NULL,
  `address_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  KEY `FK_dispatchs_users` (`user_id`),
  KEY `FK_dispatchs_carriers` (`carrier_id`),
  KEY `FK_dispatchs_addresses` (`address_id`),
  CONSTRAINT `FK_dispatchs_addresses` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`),
  CONSTRAINT `FK_dispatchs_carriers` FOREIGN KEY (`carrier_id`) REFERENCES `carriers` (`id`),
  CONSTRAINT `FK_dispatchs_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.dispatchs: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `dispatchs` DISABLE KEYS */;
INSERT INTO `dispatchs` (`id`, `user_id`, `destination`, `carrier_id`, `timestart`, `timeend`, `status`, `token`, `address_id`) VALUES
	(1, 2, '{lat: -27.592712, lng: -48.521353}', 1, '2016-11-11 20:10:00', NULL, 'Pending', 'ee157873fe5264abc17176ce6d3839bb', 1),
	(2, 1, '{lat: -27.595365, lng: -48.517555}', 2, '2016-11-11 20:14:00', NULL, 'Pending', 'e7d3e07f21889a5170a707b5e648016c', 2),
	(3, 2, '{lat: -27.592712, lng: -48.521353}', 3, '2016-11-11 20:15:00', '2017-05-18 22:18:46', 'Delivered', '09b9b2c50d6c8e144b0870dbf0c4d3f6', 3),
	(4, 1, '{lat: -27.595365, lng: -48.517555}', 1, '2016-11-11 20:14:00', NULL, 'Pending', '3bc50de24b2d31c65e334374f3c3d059', 4),
	(5, 2, '{lat:-27.5930711, lng: -48.55756550000001}', 4, '2016-11-28 18:34:00', NULL, 'Pending', '898f55a1855567176ebb0084b626aa60', 1),
	(6, 1, '{lat:-26.3044084, lng: -48.84638319999999}', 4, '2016-11-28 21:57:00', NULL, 'Pending', '0d1d9cd2e86dc51af5684557b0bdb3f7', 2),
	(7, 2, '{lat:-27.5918875, lng: -48.493988599999966}', 4, '2016-12-22 00:30:00', '2017-05-18 01:41:50', 'Delivered', '688edd05f339ca8db11adb03ef933d0d', 3),
	(8, 2, '{lat:-27.591558, lng:-48.493229}', 1, '2017-05-03 22:36:00', NULL, 'Pending', '7b4455fbcad4fa7c13bd1c0f13bf9ade', 4),
	(9, 2, '{lat:-27.587402, lng:-48.499813}', 1, '2017-05-05 23:05:00', NULL, 'Pending', 'e6fa786318320430269543cd0374bfcf', 1),
	(10, 2, '{lat:-27.587402, lng:-48.499813}', 2, '2017-05-08 23:15:00', '2017-05-18 01:31:51', 'Delivered', '034825e0742e64712dc127bcfb16a22f', 2);
/*!40000 ALTER TABLE `dispatchs` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.payments
DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.payments: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.permissions
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller` varchar(100) NOT NULL,
  `action` varchar(100) NOT NULL,
  `caption` varchar(100) NOT NULL,
  `menuitem` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `controller_action` (`controller`,`action`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.permissions: ~65 rows (aproximadamente)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `controller`, `action`, `caption`, `menuitem`) VALUES
	(1, 'CarriersController', 'index', 'List Carriers', 'Y'),
	(2, 'CarriersController', 'view', 'View Carriers', 'N'),
	(3, 'CarriersController', 'add', 'Add Carriers', 'Y'),
	(4, 'CarriersController', 'edit', 'Edit Carriers', 'N'),
	(5, 'CarriersController', 'delete', 'Delete Carriers', 'N'),
	(6, 'CompaniesController', 'index', 'List Companies', 'Y'),
	(7, 'CompaniesController', 'view', 'View Companies', 'N'),
	(8, 'CompaniesController', 'add', 'Add Companies', 'Y'),
	(9, 'CompaniesController', 'edit', 'Edit Companies', 'N'),
	(10, 'CompaniesController', 'delete', 'Delete Companies', 'N'),
	(11, 'ConfigsController', 'index', 'List Configs', 'Y'),
	(12, 'ConfigsController', 'view', 'View Configs', 'N'),
	(13, 'ConfigsController', 'add', 'Add Configs', 'Y'),
	(14, 'ConfigsController', 'edit', 'Edit Configs', 'N'),
	(15, 'ConfigsController', 'delete', 'Delete Configs', 'N'),
	(16, 'DispatchsController', 'index', 'List Dispatchs', 'Y'),
	(17, 'DispatchsController', 'view', 'View Dispatchs', 'N'),
	(18, 'DispatchsController', 'add', 'Add Dispatchs', 'Y'),
	(19, 'DispatchsController', 'edit', 'Edit Dispatchs', 'N'),
	(20, 'DispatchsController', 'delete', 'Delete Dispatchs', 'N'),
	(21, 'PermissionsController', 'index', 'List Permissions', 'Y'),
	(22, 'PermissionsController', 'view', 'View Permissions', 'N'),
	(23, 'PermissionsController', 'add', 'Add Permissions', 'Y'),
	(24, 'PermissionsController', 'edit', 'Edit Permissions', 'N'),
	(25, 'PermissionsController', 'delete', 'Delete Permissions', 'N'),
	(26, 'PermissionsRolesController', 'index', 'List PermissionsRoles', 'Y'),
	(27, 'PermissionsRolesController', 'view', 'View PermissionsRoles', 'N'),
	(28, 'PermissionsRolesController', 'add', 'Add PermissionsRoles', 'Y'),
	(29, 'PermissionsRolesController', 'edit', 'Edit PermissionsRoles', 'N'),
	(30, 'PermissionsRolesController', 'delete', 'Delete PermissionsRoles', 'N'),
	(31, 'PlansController', 'index', 'List Plans', 'Y'),
	(32, 'PlansController', 'view', 'View Plans', 'N'),
	(33, 'PlansController', 'add', 'Add Plans', 'Y'),
	(34, 'PlansController', 'edit', 'Edit Plans', 'N'),
	(35, 'PlansController', 'delete', 'Delete Plans', 'N'),
	(36, 'ResourcesController', 'index', 'List Resources', 'Y'),
	(37, 'ResourcesController', 'view', 'View Resources', 'N'),
	(38, 'ResourcesController', 'add', 'Add Resources', 'Y'),
	(39, 'ResourcesController', 'edit', 'Edit Resources', 'N'),
	(40, 'ResourcesController', 'delete', 'Delete Resources', 'N'),
	(41, 'RolesController', 'index', 'List Roles', 'Y'),
	(42, 'RolesController', 'view', 'View Roles', 'N'),
	(43, 'RolesController', 'add', 'Add Roles', 'Y'),
	(44, 'RolesController', 'edit', 'Edit Roles', 'N'),
	(45, 'RolesController', 'delete', 'Delete Roles', 'N'),
	(46, 'RolesUsersController', 'index', 'List RolesUsers', 'Y'),
	(47, 'RolesUsersController', 'view', 'View RolesUsers', 'N'),
	(48, 'RolesUsersController', 'add', 'Add RolesUsers', 'Y'),
	(49, 'RolesUsersController', 'edit', 'Edit RolesUsers', 'N'),
	(50, 'RolesUsersController', 'delete', 'Delete RolesUsers', 'N'),
	(51, 'UsersController', 'index', 'List Users', 'Y'),
	(52, 'UsersController', 'view', 'View Users', 'N'),
	(53, 'UsersController', 'add', 'Add Users', 'Y'),
	(54, 'UsersController', 'edit', 'Edit Users', 'N'),
	(55, 'UsersController', 'delete', 'Delete Users', 'N'),
	(56, 'AddressesController', 'index', 'List Addresses', 'Y'),
	(57, 'AddressesController', 'view', 'View Addresses', 'N'),
	(58, 'AddressesController', 'add', 'Add Addresses', 'Y'),
	(59, 'AddressesController', 'edit', 'Edit Addresses', 'N'),
	(60, 'AddressesController', 'delete', 'Delete Addresses', 'N'),
	(61, 'AddressesUsersController', 'index', 'List AddressesUsers', 'Y'),
	(62, 'AddressesUsersController', 'view', 'View AddressesUsers', 'N'),
	(63, 'AddressesUsersController', 'add', 'Add AddressesUsers', 'N'),
	(64, 'AddressesUsersController', 'edit', 'Edit AddressesUsers', 'N'),
	(65, 'AddressesUsersController', 'delete', 'Delete AddressesUsers', 'N');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.permissions_roles
DROP TABLE IF EXISTS `permissions_roles`;
CREATE TABLE IF NOT EXISTS `permissions_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  `self` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_id_role_id` (`permission_id`,`role_id`),
  KEY `FK_permissions_roles_roles` (`role_id`),
  CONSTRAINT `FK_permissions_roles_permissions` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  CONSTRAINT `FK_permissions_roles_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.permissions_roles: ~32 rows (aproximadamente)
/*!40000 ALTER TABLE `permissions_roles` DISABLE KEYS */;
INSERT INTO `permissions_roles` (`id`, `permission_id`, `role_id`, `self`) VALUES
	(1, 16, 2, 'N'),
	(2, 17, 2, 'N'),
	(5, 52, 2, 'N'),
	(6, 2, 2, 'N'),
	(7, 6, 3, 'N'),
	(8, 8, 3, 'N'),
	(9, 7, 3, 'N'),
	(10, 18, 3, 'N'),
	(11, 38, 3, 'N'),
	(12, 36, 3, 'N'),
	(13, 37, 3, 'N'),
	(14, 3, 3, 'N'),
	(15, 5, 3, 'N'),
	(16, 4, 3, 'N'),
	(18, 1, 3, 'N'),
	(19, 2, 3, 'N'),
	(20, 10, 3, 'N'),
	(22, 9, 3, 'N'),
	(23, 19, 3, 'N'),
	(24, 20, 3, 'N'),
	(25, 7, 2, 'N'),
	(26, 54, 2, 'N'),
	(27, 58, 3, 'N'),
	(28, 63, 3, 'N'),
	(29, 58, 2, 'Y'),
	(30, 59, 2, 'Y'),
	(31, 63, 2, 'Y'),
	(32, 64, 2, 'Y'),
	(33, 56, 3, 'N'),
	(34, 57, 3, 'N'),
	(35, 61, 3, 'N'),
	(36, 62, 3, 'N');
/*!40000 ALTER TABLE `permissions_roles` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.plans
DROP TABLE IF EXISTS `plans`;
CREATE TABLE IF NOT EXISTS `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_plans_users` (`user_id`),
  CONSTRAINT `FK_plans_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Copiando dados para a tabela lpdemilis_db.plans: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `plans` DISABLE KEYS */;
INSERT INTO `plans` (`id`, `user_id`) VALUES
	(1, 3),
	(4, 4);
/*!40000 ALTER TABLE `plans` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.resources
DROP TABLE IF EXISTS `resources`;
CREATE TABLE IF NOT EXISTS `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource` enum('Company','Carrier') NOT NULL,
  `type` enum('Free','Paid') NOT NULL DEFAULT 'Free',
  `plan_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_resources_plans` (`plan_id`),
  CONSTRAINT `FK_resources_plans` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.resources: ~24 rows (aproximadamente)
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources` (`id`, `resource`, `type`, `plan_id`) VALUES
	(1, 'Company', 'Free', 1),
	(2, 'Company', 'Free', 1),
	(3, 'Carrier', 'Free', 1),
	(4, 'Carrier', 'Free', 1),
	(5, 'Carrier', 'Free', 1),
	(6, 'Company', 'Paid', 1),
	(9, 'Company', 'Free', 4),
	(10, 'Carrier', 'Free', 4),
	(11, 'Carrier', 'Free', 4),
	(12, 'Carrier', 'Paid', 4),
	(13, 'Carrier', 'Paid', 4),
	(14, 'Company', 'Free', 1),
	(15, 'Company', 'Free', 1),
	(16, 'Company', 'Free', 1),
	(17, 'Company', 'Free', 1),
	(18, 'Company', 'Free', 1),
	(19, 'Company', 'Free', 1),
	(20, 'Company', 'Free', 1),
	(21, 'Company', 'Free', 1),
	(22, 'Company', 'Free', 1),
	(23, 'Company', 'Free', 1),
	(24, 'Company', 'Free', 1),
	(25, 'Company', 'Free', 1),
	(26, 'Company', 'Free', 1);
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.roles: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`) VALUES
	(1, 'admin'),
	(2, 'user'),
	(3, 'manager');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.roles_users
DROP TABLE IF EXISTS `roles_users`;
CREATE TABLE IF NOT EXISTS `roles_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_id_user_id` (`role_id`,`user_id`),
  KEY `FK_roles_users_users` (`user_id`),
  CONSTRAINT `FK_roles_users_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `FK_roles_users_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.roles_users: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
INSERT INTO `roles_users` (`id`, `role_id`, `user_id`) VALUES
	(1, 1, 1),
	(2, 2, 2),
	(4, 2, 3),
	(6, 2, 4),
	(3, 3, 3),
	(7, 3, 4);
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;


-- Copiando estrutura para tabela lpdemilis_db.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela lpdemilis_db.users: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `username`, `password`, `email`, `created`, `modified`) VALUES
	(1, 'Administrador', 'admin', '$2y$10$WUybDBt6T.PVFWDU3MufW.a5CGG7a0Td0g0LnvEZ.U6sDz0Kwy.iu', 'lpdemilis@gmail.com', '2016-11-11 20:01:43', '2016-11-11 20:01:43'),
	(2, 'Usuário', 'user', '$2y$10$nXLvIsbnt9qFx9bi3I5BOudgUTsqIivdTBOlE9ATrLMckOMktT9VW', 'lpdemilis@gmail.com', '2016-11-11 20:02:59', '2016-11-16 15:52:03'),
	(3, 'Manager 1', 'manager1', '$2y$10$HpCHJlxwg9EcZkskmbZfBOI/L62eEFUDxxHU2vkRA02PdidFc1eqm', 'lpdemilis@gmail.com', '2016-11-16 18:51:12', '2016-11-19 01:29:30'),
	(4, 'Manager 2', 'manager2', '$2y$10$ciAyBqGqQtpOslOmg4XMKemPsB.DucDnYBrNCeI1.byyUJ2z.bwwK', 'lpdemilis@gmail.com', '2016-11-19 01:30:16', '2016-11-19 01:30:36');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Copiando estrutura para trigger lpdemilis_db.carriers_before_insert
DROP TRIGGER IF EXISTS `carriers_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `carriers_before_insert` BEFORE INSERT ON `carriers` FOR EACH ROW BEGIN
	SET @resource = (SELECT r.resource
					  	  FROM resources r
						  WHERE r.id = NEW.resource_id);
	
   IF (@resource <> 'Carrier') THEN
   	SIGNAL SQLSTATE '07006' SET MESSAGE_TEXT = 'restricted datatype attribute violation';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Copiando estrutura para trigger lpdemilis_db.carriers_before_update
DROP TRIGGER IF EXISTS `carriers_before_update`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `carriers_before_update` BEFORE UPDATE ON `carriers` FOR EACH ROW BEGIN
	SET @resource = (SELECT r.resource
					  	  FROM resources r
						  WHERE r.id = NEW.resource_id);
	
   IF (@resource <> 'Carrier') THEN
   	SIGNAL SQLSTATE '07006' SET MESSAGE_TEXT = 'restricted datatype attribute violation';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Copiando estrutura para trigger lpdemilis_db.companies_before_insert
DROP TRIGGER IF EXISTS `companies_before_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `companies_before_insert` BEFORE INSERT ON `companies` FOR EACH ROW BEGIN
	SET @resource = (SELECT r.resource
					  	  FROM resources r
						  WHERE r.id = NEW.resource_id);
	
   IF (@resource <> 'Company') THEN
   	SIGNAL SQLSTATE '07006' SET MESSAGE_TEXT = 'restricted datatype attribute violation';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Copiando estrutura para trigger lpdemilis_db.companies_before_update
DROP TRIGGER IF EXISTS `companies_before_update`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `companies_before_update` BEFORE UPDATE ON `companies` FOR EACH ROW BEGIN
	SET @resource = (SELECT r.resource
					  	  FROM resources r
						  WHERE r.id = NEW.resource_id);
	
   IF (@resource <> 'Company') THEN
   	SIGNAL SQLSTATE '07006' SET MESSAGE_TEXT = 'restricted datatype attribute violation';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
