<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dispatchs Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Carriers
 * @property \Cake\ORM\Association\BelongsTo $Addresses
 *
 * @method \App\Model\Entity\Dispatch get($primaryKey, $options = [])
 * @method \App\Model\Entity\Dispatch newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Dispatch[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Dispatch|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Dispatch patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Dispatch[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Dispatch findOrCreate($search, callable $callback = null, $options = [])
 */
class DispatchsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dispatchs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Carriers', [
            'foreignKey' => 'carrier_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Addresses', [
            'foreignKey' => 'address_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('destination', 'create')
            ->notEmpty('destination');

        $validator
            ->dateTime('timestart')
            ->requirePresence('timestart', 'create')
            ->notEmpty('timestart');

        $validator
            ->dateTime('timeend')
            ->allowEmpty('timeend');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->requirePresence('token', 'create')
            ->notEmpty('token')
            ->add('token', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('company_id', 'create')
            ->notEmpty('company_id')
            ->notEquals('company_id', 0);

        $validator
            ->requirePresence('carrier_id', 'create')
            ->notEmpty('carrier_id')
            ->notEquals('carrier_id', 0);

        $validator
            ->requirePresence('user_id', 'create')
            ->notEmpty('user_id')
            ->notEquals('user_id', 0);

        $validator
            ->notEmpty('user_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['token']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['carrier_id'], 'Carriers'));
        $rules->add($rules->existsIn(['address_id'], 'Addresses'));

        return $rules;
    }
}
