<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Addresses Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Users
 *
 * @method \App\Model\Entity\Address get($primaryKey, $options = [])
 * @method \App\Model\Entity\Address newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Address[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Address|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Address patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Address[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Address findOrCreate($search, callable $callback = null, $options = [])
 */
class AddressesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('addresses');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsToMany('Users', [
            'foreignKey' => 'address_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'addresses_users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->numeric('lat')
            ->requirePresence('lat', 'create')
            ->notEmpty('lat');

        $validator
            ->numeric('lng')
            ->requirePresence('lng', 'create')
            ->notEmpty('lng');

        $validator
            ->requirePresence('street_number', 'create')
            ->notEmpty('street_number');

        $validator
            ->requirePresence('route', 'create')
            ->notEmpty('route');

        $validator
            ->requirePresence('sublocality_level_1', 'create')
            ->notEmpty('sublocality_level_1');

        $validator
            ->requirePresence('locality', 'create')
            ->notEmpty('locality');

        $validator
            ->requirePresence('administrative_area_level_2', 'create')
            ->notEmpty('administrative_area_level_2');

        $validator
            ->requirePresence('administrative_area_level_1', 'create')
            ->notEmpty('administrative_area_level_1');

        $validator
            ->requirePresence('country', 'create')
            ->notEmpty('country');

        $validator
            ->requirePresence('postal_code', 'create')
            ->notEmpty('postal_code');

        return $validator;
    }
}
