<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dispatch Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $destination
 * @property int $carrier_id
 * @property \Cake\I18n\Time $timestart
 * @property \Cake\I18n\Time $timeend
 * @property string $status
 * @property string $token
 * @property int $address_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Carrier $carrier
 */
class Dispatch extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

//    /**
//     * Fields that are excluded from JSON versions of the entity.
//     *
//     * @var array
//     */
//    protected $_hidden = [
//        'token'
//    ];
}
