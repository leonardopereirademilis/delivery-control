<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Carrier Entity
 *
 * @property int $id
 * @property string $name
 * @property string $base_position
 * @property string $position
 * @property string $status
 * @property \Cake\I18n\Time $datemodified
 * @property int $company_id
 * @property string $imei
 * @property int $resource_id
 *
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Resource $resource
 * @property \App\Model\Entity\Dispatch[] $dispatchs
 */
class Carrier extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
