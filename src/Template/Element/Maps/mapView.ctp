<div id="map"></div>

<div class="table-responsive">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>
                Icon
            </th>
            <th>
                Legend
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <img src="/delivery-control/img/origin.png">
            </td>
            <td>
                Origem da entrega
            </td>
        </tr>
        <tr>
            <td>
                <img src="/delivery-control/img/destination.png">
            </td>
            <td>
                Destino da entrega
            </td>
        </tr>
        <tr>
            <td>
                <img src="/delivery-control/img/carrier.png">
            </td>
            <td>
                Posição do entregador
            </td>
        </tr>
        <tr>
            <td>
                <img src="/delivery-control/img/glyphicons_free/glyphicons/png/glyphicons-351-shopping-bag.png">
            </td>
            <td>
                Entregas próximas
            </td>
        </tr>
        </tbody>
    </table>
</div>

<script>

    var map;
    var markers = [];
    var infos = [];
    var directionsService;
    var directionsDisplay;
    var interval;
    var carrier;
    var carrierInfo;

    var myLatLngOrigin = <?php
                            if(isset($dispatch)) {
                                if ($dispatch->carrier->base_position == "") {
                                    echo h($dispatch->carrier->company->position);
                                } else {
                                    echo h($dispatch->carrier->base_position);
                                }
                            }else{
                                echo 'null';
                            }
                         ?>;
    var myLatLngDestination = <?php
                                if(isset($dispatch)) {
                                    echo h($dispatch->destination);
                                }else {
                                    echo 'null';
                                }
                              ?>;

    var myDispatch = [myLatLngOrigin, myLatLngDestination];

    function initMap() {
        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        directionsDisplay.setMap(map);

        interval = setInterval(function(){ updateCarrierPosition() }, 5000);

        <?php
            if(isset($dispatch->dispatchs)) {
                echo "centerMapToPosition(myLatLngDestination);";
                foreach ($dispatch->dispatchs as $d) {
                    if ($d->id != $dispatch->id) {
                        echo "addMarker($d->destination, '/delivery-control/img/glyphicons_free/glyphicons/png/glyphicons-351-shopping-bag.png', 'Entrega próxima');";
                    }
                }
                echo "calculateAndDisplayRoute(directionsService, directionsDisplay, myDispatch);";
            }elseif(isset($carrier->dispatchs)){
                echo "centerMapToPosition(" . $carrier->position . ");";
                foreach ($carrier->dispatchs as $d) {
                    echo "addMarker($d->destination, '/delivery-control/img/glyphicons_free/glyphicons/png/glyphicons-351-shopping-bag.png', 'Entrega próxima');";
                }
            }
        ?>

        <?php
            if(isset($dispatch->carrier)) {
                if (($dispatch->carrier->position) && ($dispatch->carrier->status == "Online")) {
                    $carrier_position = h($dispatch->carrier->position);
                    $carrier_visible = "true";
                } else {
                    $carrier_position = h($dispatch->carrier->company->position);
                    $carrier_visible = "false";
                }
            }elseif(isset($carrier->id)){
                if (($carrier->position) && ($carrier->status == "Online")) {
                    $carrier_position = h($carrier->position);
                    $carrier_visible = "true";
                } else {
                    $carrier_position = h($carrier->company->position);
                    $carrier_visible = "false";
                }
            }
        ?>

        carrier = new google.maps.Marker({
            position: <?= h($carrier_position) ?>,
            visible: <?= $carrier_visible ?>,
            map: map,
            icon: '/delivery-control/img/carrier.png'
        });

        carrierInfo = new google.maps.InfoWindow({
            <?php
                if(isset($dispatch->carrier->name)) {
                    echo "content: '".__("Carrier").": <b>".h($dispatch->carrier->name)."</b>'";
                }elseif(isset($carrier->name)){
                    echo "content: '".__("Carrier").": <b>".h($carrier->name)."</b>'";
                }
            ?>
        });

        carrier.addListener('click', function() {
            carrierInfo.open(map, carrier);
        });

    }

    // Adds a marker to the map and push to the array.
    function addMarker(location,icon,contentString) {
        if(icon != ""){
            icon = icon;
        }
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: icon
        });

        if (contentString != "") {
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });

            infos.push(infowindow);
        }

        markers.push(marker);
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
        setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay, wayPoints) {
        var waypts = [];
        if(wayPoints.length > 2) {
            for (var i = 1; i < wayPoints.length - 1; i++) {
                waypts.push({
                    location: wayPoints[i],
                    stopover: true
                });
            }
        }

        directionsService.route({
            origin: wayPoints[0],
            destination: wayPoints[wayPoints.length - 1],
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });

        addMarker(myLatLngOrigin, '/delivery-control/img/origin.png', 'Origem da entrega');
        addMarker(myLatLngDestination, '/delivery-control/img/destination.png', 'Destino da entrega');

        //var originInfoWindow = new google.maps.InfoWindow({map: map});
        //originInfoWindow.setPosition(myLatLngOrigin);
        //originInfoWindow.setContent('Origin');

        //var destinationInfoWindow = new google.maps.InfoWindow({map: map});
        //destinationInfoWindow.setPosition(myLatLngDestination);
        //destinationInfoWindow.setContent('Destination');
    }

    function centerMapToMyPosition(){
        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                map.setCenter(pos);
            });
        }
    }

    function centerMapToPosition(position){
        map.setCenter(position);
    }

    function updateCarrierPosition(){
        $.get(
            <?php
                if(isset($dispatch->carrier)) {
                    echo '"/delivery-control/dispatchs/ajaxMsg/" + '. $dispatch->id .',';
                }elseif(isset($carrier->id)){
                    echo '"/delivery-control/carriers/ajaxMsg/" + '. $carrier->id .',';
                }
            ?>
            null,
            function(data) {
                var parsed_data = JSON.parse(data);
                <?php
                if(isset($dispatch->carrier)) {
                    echo "var destination = parsed_data.carrier.position;";
                }elseif(isset($carrier->id)){
                    echo "var destination = parsed_data.position;";
                }
                ?>
                destination = destination.replace("{","");
                destination = destination.replace("}","");
                destination = destination.replace("lat:","");
                destination = destination.replace("lng:","");
                destination = destination.replace(" ","");
                var destination_arr = destination.split(",");
                var pos = {
                    lat: parseFloat(destination_arr[0]),
                    lng: parseFloat(destination_arr[1])
                };
                carrier.setPosition(pos);
                carrier.setVisible(true);
                <?php
                    if(isset($dispatch->carrier)) {
                        echo "updateDispatch(parsed_data);";
                    }
                ?>
            }
        );
    }

    function updateDispatch(dispatch){
        $("#dispatch_status_div").html("<p>" + dispatch.status + "</p>");

        if(dispatch.status != "Pending"){
            $("#dispatch_timeend_div").html(dispatch.timeend.toLocaleString() );
        }else{
            $("#dispatch_timeend_div").html("");
        }

    }

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrvkDh4Efr0RQq19JTjIP7FaX0UHPhWBM&callback=initMap"
        async defer></script>