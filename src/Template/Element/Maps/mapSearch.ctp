<style>
    .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
        margin-top: 7px;;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container {
        font-family: Roboto;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    #target {
        width: 345px;
    }
</style>

<input id="pac-input" class="controls" type="text" placeholder="Search Box">

<div id="map"></div>

<script>
    var map;
    var dispatch;
    var company;
    var markers = [];
    var newMarkers = [];
    var infos = [];

    function initAutocomplete() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        <?php
        if (isset($dispatch->destination)) {
            echo "map.setCenter($dispatch->destination);";
            echo "addMarker($dispatch->destination, '/delivery-control/img/glyphicons_free/glyphicons/png/glyphicons-351-shopping-bag.png', 'Entrega próxima');";
        } elseif (isset($company->position)) {
            echo "map.setCenter($company->position);";
            echo "addMarker($company->position, '/delivery-control/img/origin.png', 'Origem da entrega');";
        } elseif (isset($address->id)) {
            echo "map.setCenter({lat: $address->lat, lng: $address->lng});";
            echo "addMarker({lat: $address->lat, lng: $address->lng}, '/delivery-control/img/glyphicons_free/glyphicons/png/glyphicons-243-map-marker.png', '" . __('Addres') . "');";
        } else {
            echo "centerMapToMyPosition();";
        }
        ?>

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        markers = [];
        newMarkers = [];
        // [START region_getplaces]
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            newMarkers.forEach(function (newMarker) {
                newMarker.setMap(null);
            });
            newMarkers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                // Create a marker for each place.
                newMarkers.push(new google.maps.Marker({
                    map: map,
                    icon: "/delivery-control/img/destination.png", //icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }

                var latlng_str = place.geometry.location.toString();
                latlng_str = latlng_str.replace("(", "{lat:");
                latlng_str = latlng_str.replace(",", ", lng:");
                latlng_str = latlng_str.replace(")", "}");

                $("#destination").val(latlng_str);
                $("#lat").val(place.geometry.location.lat());
                $("#lng").val(place.geometry.location.lng());
                $("#position").val(latlng_str);

                $("#address").val(place.formatted_address);

                $("#street-number").val("");
                $("#route").val("");
                $("#sublocality-level-1").val("");
                $("#locality").val("");
                $("#administrative-area-level-2").val("");
                $("#administrative-area-level-1").val("");
                $("#country").val("");
                $("#postal-code").val("");

                console.log(place.address_components);

                place.address_components.forEach(function (component) {
                    var fieldname = "#"+ component.types[0].toString().replace(/_/g, "-");
                    if(component.long_name.trim() != ""){
                        $(fieldname).attr('readonly', 'readonly');
                        $(fieldname).val(component.long_name);
                    }

                    if((component.types[0].toString() == "postal_code_prefix") && ($("#postal-code").val() == "")){
                        $("#postal-code").val(component.long_name);
                    }

                    if(component.types[0].toString() == "premise"){
                        $("#name").val(component.long_name);
                    }

                });

//                if ($("#street-number").val() == ""){
//                    $("#street-number").removeAttr('readonly');
//                    $("#street-number").attr("placeholder", "Type a Street Number");
//                }
//
//                if ($("#route").val() == ""){
//                    $("#route").removeAttr('readonly');
//                    $("#route").attr("placeholder", "Type a Route");
//                }
//
//                if ($("#sublocality-level-1").val() == ""){
//                    $("#sublocality-level-1").removeAttr('readonly');
//                    $("#sublocality-level-1").attr("placeholder", "Type a Sublocality Level 1");
//                }
//
//                if ($("#locality").val() == ""){
//                    $("#locality").removeAttr('readonly');
//                    $("#locality").attr("placeholder", "Type a Locality");
//                }
//
//                if ($("#administrative-area-level-2").val() == ""){
//                    $("#administrative-area-level-2").removeAttr('readonly');
//                    $("#administrative-area-level-2").attr("placeholder", "Type an Administrative Area Level 2");
//                }
//
//                if ($("#administrative-area-level-1").val() == ""){
//                    $("#administrative-area-level-1").removeAttr('readonly');
//                    $("#administrative-area-level-1").attr("placeholder", "Type an Administrative Area Level 1");
//                }
//
//                if ($("#country").val() == ""){
//                    $("#country").removeAttr('readonly');
//                    $("#country").attr("placeholder", "Type a Country");
//                }
//
//                if ($("#postal-code").val() == ""){
//                    $("#postal-code").removeAttr('readonly');
//                    $("#postal-code").attr("placeholder", "Type a Postal Code Prefix");
//                }

                map.setZoom(16);
//                markers = [];
            });
            map.fitBounds(bounds);
        });
        // [END region_getplaces]
    }

    function centerMapToMyPosition() {
        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                map.setCenter(pos);
            });
        }
    }

    // Adds a marker to the map and push to the array.
    function addMarker(location, icon, contentString) {
        if (icon != "") {
            icon = icon;
        }
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: icon
        });

        if (contentString != "") {
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });

            infos.push(infowindow);
        }

        markers.push(marker);
    }

    function selectAddress(lat, lng, id) {
        var pos = {
            lat: lat,
            lng: lng
        };
        map.setCenter(pos);

        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }

        var marker = new google.maps.Marker({
            position: pos,
            map: map,
            icon: "/delivery-control/img/destination.png"
        });

        //markers.push(marker);

        var latlng_str = "{lat:" + lat + ", lng:" + lng + "}";

        $("#destination").val(latlng_str);
        $("#lat").val(lat);
        $("#lng").val(lng);

        map.setZoom(16);
    }

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrvkDh4Efr0RQq19JTjIP7FaX0UHPhWBM&libraries=places&callback=initAutocomplete"
        async defer></script>