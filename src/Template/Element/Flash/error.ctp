<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-danger alert-dismissable flash-message">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <span class="glyphicon glyphicon-exclamation-sign"></span> <?= $message ?>
</div>
