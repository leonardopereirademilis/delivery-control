<?php
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-info alert-dismissable flash-message">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
    <span class="glyphicon glyphicon-info-sign"></span> <?= $message ?>
</div>
