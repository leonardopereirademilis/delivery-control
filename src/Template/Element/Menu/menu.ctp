<script>

    <?php
        $i = 0;
        $index = 0;
        foreach ($menu as $controller => $actions){
            if($controller === $this->request->controller.'Controller'){
                $index = $i;
                break;
            }
            $i++;
        }
    ?>

    var index = <?=$index?>;

    $( function() {
        $( "#accordion-menu" ).accordion({
            collapsible: true,
            heightStyle: "content",
            active: index
        });
    } );
</script>

<?php
    echo '<h4>Menu</h4>';
    echo '<div id="accordion-menu">';

    foreach ($menu as $controller => $actions){
        $controller_str = str_replace('Controller', '', $controller);
        echo '<h3><span class="glyphicon glyphicon-' . strtolower($controller_str) . '"></span> <span class="menu-title">' . __($controller_str) . '</span></h3>';
//        echo '<h3><span class="glyphicon glyphicon-user"></span> <span class="menu-title">' . __($controller_str) . '</span></h3>';
        echo '<div>';

        foreach ($actions as $action => $caption){
            $li_class = '';

            if(($controller === $this->request->controller.'Controller') && ($action === $this->request->action)){
                $li_class = 'class="selected-action"';
            }
//            if(($controller === "UsersController") && (($action == 'view') || ($action == 'edit'))){
//                echo '<li>' . $this->Html->link(__($action_name . ' ' . $controller_str), ['controller' => $controller_str, 'action' => $action, $user->id]) . '</li>';
//            }else{
                echo '<li '.$li_class.'>' . $this->Html->link(ucfirst(__($caption)), ['controller' => $controller_str, 'action' => $action]) . '</li>';
//            }
        }

        echo '</div>';

    }

    echo '</div>';
?>