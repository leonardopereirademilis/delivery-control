<script>

    <?php
        $i = 0;
        $index = 0;
        foreach ($menu as $controller => $actions){
            if($controller === $this->request->controller.'Controller'){
                $index = $i;
                break;
            }
            $i++;
        }
    ?>

</script>

<?php

    echo '    <li><a href="/delivery-control">Home</a></li>';

    if ((($this->request->controller == "Users") && (($this->request->action == "login") || (($this->request->action == "add") && (!isset($this->request->Session()->read('Auth')['User']['id']))))) || (!isset($menu))) {
       //menu não logado
    }else{
        foreach ($menu as $controller => $actions) {
            $controller_str = str_replace('Controller', '', $controller);

            echo '<li class="dropdown">';
            echo '    <a class="dropdown-toggle" data-toggle="dropdown" href="#">' . __($controller_str) . '<span class="caret"></span></a>';
            echo '    <ul class="dropdown-menu">';

            foreach ($actions as $action => $caption) {
                echo '<li>' . $this->Html->link(ucfirst(__($caption)), ['controller' => $controller_str, 'action' => $action]) . '</li>';
            }

            echo '    </ul>';
            echo '</li>';

        }
    }
?>