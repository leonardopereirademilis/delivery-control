<div class="text-center white">
    <h2>SERVICES</h2>
    <h4>What we offer</h4>
    <br>
    <div class="row">
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-off logo-small"></span>
            <h4>POWER</h4>
            <p>Lorem ipsum dolor sit amet..</p>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-heart logo-small"></span>
            <h4>LOVE</h4>
            <p>Lorem ipsum dolor sit amet..</p>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-lock logo-small"></span>
            <h4>JOB DONE</h4>
            <p>Lorem ipsum dolor sit amet..</p>
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-leaf logo-small"></span>
            <h4>GREEN</h4>
            <p>Lorem ipsum dolor sit amet..</p>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-certificate logo-small"></span>
            <h4>CERTIFIED</h4>
            <p>Lorem ipsum dolor sit amet..</p>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-wrench logo-small"></span>
            <h4>HARD WORK</h4>
            <p>Lorem ipsum dolor sit amet..</p>
        </div>
    </div>
</div>

<div class="gray">
    <div class="text-center">
        <h2>Pricing</h2>
        <h4>Choose a payment plan that works for you</h4>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h1>Plano 1</h1>
                </div>
                <div class="panel-body">
                    <p>Quantidade de carregadores: <strong>1 (um)</strong></p>
                    <p>Quantidade de empresas: <strong>1 (uma)</strong></p>
                    <p>Valor da parcela (R$): <strong>10,00</strong></p>
                    <p>Frequência das cobranças: <strong>mensal</strong></p>
                    <p>Duração das adesões: <strong>sem expiração</strong></p>
                </div>
                <div class="panel-footer">
                    <h3>R$ 10,00</h3>
                    <h4>por mês</h4>
                    <!-- INICIO FORMULARIO BOTAO PAGSEGURO: NAO EDITE OS COMANDOS DAS LINHAS ABAIXO -->
                    <form action="https://pagseguro.uol.com.br/pre-approvals/request.html" method="post" class="form-sign-up">
                        <input type="hidden" name="code" value="EB44C3F6CDCDD8B334277FBB63EA692C" />
                        <input type="hidden" name="iot" value="button" />
                        <input type="submit" name="submit" class="btn btn-lg" value="Contratar">
                    </form>
                    <!-- FINAL FORMULARIO BOTAO PAGSEGURO -->
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h1>Plano 5</h1>
                </div>
                <div class="panel-body">
                    <p>Quantidade de carregadores: <strong>5 (cinco)</strong></p>
                    <p>Quantidade de empresas: <strong>1 (uma)</strong></p>
                    <p>Valor da parcela (R$): <strong>50,00</strong></p>
                    <p>Frequência das cobranças: <strong>mensal</strong></p>
                    <p>Duração das adesões: <strong>sem expiração</strong></p>
                </div>
                <div class="panel-footer">
                    <h3>R$ 50,00</h3>
                    <h4>por mês</h4>
                    <!-- INICIO FORMULARIO BOTAO PAGSEGURO: NAO EDITE OS COMANDOS DAS LINHAS ABAIXO -->
                    <form action="https://pagseguro.uol.com.br/pre-approvals/request.html" method="post" class="form-sign-up">
                        <input type="hidden" name="code" value="EB44C3F6CDCDD8B334277FBB63EA692C" />
                        <input type="hidden" name="iot" value="button" />
                        <input type="submit" name="submit" class="btn btn-lg" value="Contratar">
                    </form>
                    <!-- FINAL FORMULARIO BOTAO PAGSEGURO -->
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h1>Plano 10</h1>
                </div>
                <div class="panel-body">
                    <p>Quantidade de carregadores: <strong>10 (dez)</strong></p>
                    <p>Quantidade de empresas: <strong>1 (uma)</strong></p>
                    <p>Valor da parcela (R$): <strong>100,00</strong></p>
                    <p>Frequência das cobranças: <strong>mensal</strong></p>
                    <p>Duração das adesões: <strong>sem expiração</strong></p>
                </div>
                <div class="panel-footer">
                    <h3>R$ 100,00</h3>
                    <h4>por mês</h4>
                    <!-- INICIO FORMULARIO BOTAO PAGSEGURO: NAO EDITE OS COMANDOS DAS LINHAS ABAIXO -->
                    <form action="https://pagseguro.uol.com.br/pre-approvals/request.html" method="post" class="form-sign-up">
                        <input type="hidden" name="code" value="EB44C3F6CDCDD8B334277FBB63EA692C" />
                        <input type="hidden" name="iot" value="button" />
                        <input type="submit" name="submit" class="btn btn-lg" value="Contratar">
                    </form>
                    <!-- FINAL FORMULARIO BOTAO PAGSEGURO -->
                </div>
            </div>
        </div>
    </div>
</div>