<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= h($address->name) ?></h3>
    <div class="table-responsive"><table class="table table-striped table-bordered">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($address->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($address->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Street Number') ?></th>
            <td><?= h($address->street_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Route') ?></th>
            <td><?= h($address->route) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sublocality Level 1') ?></th>
            <td><?= h($address->sublocality_level_1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Locality') ?></th>
            <td><?= h($address->locality) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Administrative Area Level 2') ?></th>
            <td><?= h($address->administrative_area_level_2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Administrative Area Level 1') ?></th>
            <td><?= h($address->administrative_area_level_1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country') ?></th>
            <td><?= h($address->country) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Postal Code Prefix') ?></th>
            <td><?= h($address->postal_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($address->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lat') ?></th>
            <td><?= $this->Number->format($address->lat) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lng') ?></th>
            <td><?= $this->Number->format($address->lng) ?></td>
        </tr>
    </table></div>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($address->users)): ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Username') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($address->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->name) ?></td>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td class="actions">
                    <?php
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['UsersController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['controller' => 'Users', 'action' => 'view', $users->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['UsersController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['UsersController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                        }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table></div>
        <?php else: ?>
            <div class="alert alert-info">No record found.</div>
        <?php endif; ?>
    </div>
</div>