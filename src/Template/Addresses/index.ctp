<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Addresses') ?></h3>
    <?php if (count($addresses) > 0) { ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('address') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('lat') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('lng') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('street_number') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('route') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('sublocality_level_1') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('locality') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('administrative_area_level_2') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('administrative_area_level_1') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('country') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('postal_code') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($addresses as $address): ?>
                <tr>
                    <td><?= $this->Number->format($address->id) ?></td>
                    <td><?= h($address->name) ?></td>
                    <td><?= h($address->address) ?></td>
                    <td><?= $this->Number->format($address->lat) ?></td>
                    <td><?= $this->Number->format($address->lng) ?></td>
                    <td><?= h($address->street_number) ?></td>
                    <td><?= h($address->route) ?></td>
                    <td><?= h($address->sublocality_level_1) ?></td>
                    <td><?= h($address->locality) ?></td>
                    <td><?= h($address->administrative_area_level_2) ?></td>
                    <td><?= h($address->administrative_area_level_1) ?></td>
                    <td><?= h($address->country) ?></td>
                    <td><?= h($address->postal_code) ?></td>
                    <td class="actions">
                        <?php
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['AddressesController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['action' => 'view', $address->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['AddressesController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['action' => 'edit', $address->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['AddressesController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['action' => 'delete', $address->id], ['confirm' => __('Are you sure you want to delete # {0}?', $address->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                            }
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table></div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    <?php } else { ?>
        <div class="alert alert-info">
            No addresses found.
        </div>
    <?php }?>
</div>