    <?= $this->Form->create($address) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Edit Address') ?></h3>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('address', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('lat', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('lng', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('street_number');
            echo $this->Form->input('route');
            echo $this->Form->input('sublocality_level_1');
            echo $this->Form->input('locality');
            echo $this->Form->input('administrative_area_level_2');
            echo $this->Form->input('administrative_area_level_1');
            echo $this->Form->input('country');
            echo $this->Form->input('postal_code');
            echo $this->Form->input('users._ids', ['options' => $users]);
        ?>
    </div>

    <?php echo $this->element('/Maps/mapSearch'); ?>

    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>