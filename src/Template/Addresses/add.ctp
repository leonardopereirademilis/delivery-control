<?php
    if(isset($this->request->query['noheader'])){
        echo '<style>.navbar {display: none !important;} footer {display: none !important;} h3 {display: none !important;} .main-content {padding-left: 0px !important;} </style>';
    }
?>

    <?= $this->Form->create($address) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Add Address') ?></h3>
        <?php
            if(isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin'])) {
                echo $this->Form->input('users._ids', ['options' => $users]);
            }else{
                if(isset($this->request->query['userid'])){
                    echo $this->Form->input('username', ['readonly', 'value' => $this->request->query['username']]);
                    echo $this->Form->hidden('users._ids', ['value' => $this->request->query['userid'], 'id' => 'users-ids']);
                    echo $this->Form->hidden('userid', ['value' => $this->request->query['userid']]);
                }else {
                    echo $this->Form->hidden('users._ids', ['value' => $this->request->Session()->read('Auth')['User']['id'], 'id' => 'users-ids']);
                }
            }

            echo $this->Form->hidden('return', ['value' => $this->request->query['return']]);
            echo $this->Form->input('name');
            echo $this->Form->input('address', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('lat', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('lng', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('street_number', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('route', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('sublocality_level_1', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('locality', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('administrative_area_level_2', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('administrative_area_level_1', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('country', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
            echo $this->Form->input('postal_code', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
        ?>
    </div>

    <?php echo $this->element('/Maps/mapSearch'); ?>

    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>