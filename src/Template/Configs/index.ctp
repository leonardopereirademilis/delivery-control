<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Configs') ?></h3>
    <?php if (count($dispatchs) > 0) { ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($configs as $config): ?>
                <tr>
                    <td><?= $this->Number->format($config->id) ?></td>
                    <td><?= h($config->name) ?></td>
                    <td><?= h($config->value) ?></td>
                    <td class="actions">
                        <?php
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['ConfigsController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['action' => 'view', $config->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['ConfigsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['action' => 'edit', $config->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['ConfigsController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['action' => 'delete', $config->id], ['confirm' => __('Are you sure you want to delete # {0}?', $config->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table></div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    <?php } else { ?>
        <div class="alert alert-info">
            No configs found.
        </div>
    <?php }?>
</div>