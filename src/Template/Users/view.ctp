<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= h($user->name) ?></h3>
    <div class="table-responsive"><table class="table table-striped table-bordered">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <?php
            if(((isset($this->request->Session()->read('Auth')['User']['allpermissions']['UsersController']['edit'])) && ($user->id == $this->request->Session()->read('Auth')['User']['id'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                ?>
                <tr>
                    <th scope="row"><?= __('Username') ?></th>
                    <td><?= h($user->username) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($user->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($user->modified) ?></td>
                </tr>
                <?php
            }
        ?>
    </table></div>
    <?php
        if(((isset($this->request->Session()->read('Auth')['User']['allpermissions']['UsersController']['edit'])) && ($user->id == $this->request->Session()->read('Auth')['User']['id'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
            ?>
                <div class="related">
                    <h4><?= __('Related Addresses') ?></h4>
                    <?php if (!empty($user->addresses)): ?>
                        <div class="table-responsive"><table class="table table-striped table-bordered">
                                <tr>
                                    <th scope="col"><?= __('Id') ?></th>
                                    <th scope="col"><?= __('Name') ?></th>
                                    <th scope="col"><?= __('Address') ?></th>
                                    <th scope="col"><?= __('Lat') ?></th>
                                    <th scope="col"><?= __('Lng') ?></th>
                                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                                </tr>
                                <?php foreach ($user->addresses as $addresses): ?>
                                    <tr>
                                        <td><?= h($addresses->id) ?></td>
                                        <td><?= h($addresses->name) ?></td>
                                        <td><?= h($addresses->address) ?></td>
                                        <td><?= h($addresses->lat) ?></td>
                                        <td><?= h($addresses->lng) ?></td>
                                        <td class="actions">
                                            <?php
                                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['AddressesController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['controller' => 'Addresses', 'action' => 'view', $addresses->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                                            }
                                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['AddressesController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['controller' => 'Addresses', 'action' => 'edit', $addresses->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                                            }
                                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['AddressesController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                                echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['controller' => 'Addresses', 'action' => 'delete', $addresses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $addresses->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table></div>
                    <?php else: ?>
                        <div class="alert alert-info">No record found.</div>
                    <?php endif; ?>
                    <?= $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-plus')).' '.__('Add address'), ['controller' => 'Addresses', 'action' => 'add', 'user_id' => $user->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]); ?>
                </div>
                <div class="related">
                    <h4><?= __('Related Dispatchs') ?></h4>
                    <?php if (!empty($user->dispatchs)): ?>
                    <div class="table-responsive"><table class="table table-striped table-bordered">
                        <tr>
                            <th scope="col"><?= __('Id') ?></th>
                            <th scope="col"><?= __('Destination') ?></th>
                            <th scope="col"><?= __('Carrier Id') ?></th>
                            <th scope="col"><?= __('Timestart') ?></th>
                            <th scope="col"><?= __('Timeend') ?></th>
                            <th scope="col"><?= __('Status') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($user->dispatchs as $dispatchs): ?>
                        <tr>
                            <td><?= h($dispatchs->id) ?></td>
                            <td><?= h($dispatchs->destination) ?></td>
                            <td><?= h($dispatchs->carrier_id) ?></td>
                            <td><?= h($dispatchs->timestart) ?></td>
                            <td><?= h($dispatchs->timeend) ?></td>
                            <td><?= h($dispatchs->status) ?></td>
                            <td class="actions">
                                <?php
                                    if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                        echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['controller' => 'Dispatchs', 'action' => 'view', $dispatchs->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                                    }
                                    if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                        echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['controller' => 'Dispatchs', 'action' => 'edit', $dispatchs->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                                    }
                                    if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                        echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['controller' => 'Dispatchs', 'action' => 'delete', $dispatchs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dispatchs->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table></div>
                    <?php else: ?>
                        <div class="alert alert-info">No record found.</div>
                    <?php endif; ?>
                </div>
                <div class="related">
                    <h4><?= __('Related Plans') ?></h4>
                    <?php if (!empty($user->plans)): ?>
                    <div class="table-responsive"><table class="table table-striped table-bordered">
                        <tr>
                            <th scope="col"><?= __('Id') ?></th>
                            <th scope="col"><?= __('User Id') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($user->plans as $plans): ?>
                        <tr>
                            <td><?= h($plans->id) ?></td>
                            <td><?= h($plans->user_id) ?></td>
                            <td class="actions">
                                <?php
                                    if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['PlansController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                        echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['controller' => 'Plans', 'action' => 'view', $plans->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                                    }
                                    if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['PlansController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                        echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['controller' => 'Plans', 'action' => 'edit', $plans->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                                    }
                                    if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['PlansController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                        echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['controller' => 'Plans', 'action' => 'delete', $plans->id], ['confirm' => __('Are you sure you want to delete # {0}?', $plans->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table></div>
                    <?php else: ?>
                        <div class="alert alert-info">No record found.</div>
                    <?php endif; ?>
                </div>
                <div class="related">
                    <h4><?= __('Related Roles') ?></h4>
                    <?php if (!empty($user->roles)): ?>
                    <div class="table-responsive"><table class="table table-striped table-bordered">
                        <tr>
                            <th scope="col"><?= __('Id') ?></th>
                            <th scope="col"><?= __('Name') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($user->roles as $roles): ?>
                        <tr>
                            <td><?= h($roles->id) ?></td>
                            <td><?= h($roles->name) ?></td>
                            <td class="actions">
                                <?php
                                    if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['RolesController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                        echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['controller' => 'Roles', 'action' => 'view', $roles->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                                    }
                                    if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['RolesController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                        echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['controller' => 'Roles', 'action' => 'edit', $roles->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                                    }
                                    if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['RolesController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                        echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['controller' => 'Roles', 'action' => 'delete', $roles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $roles->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table></div>
                    <?php else: ?>
                        <div class="alert alert-info">No record found.</div>
                    <?php endif; ?>
                </div>
            <?php
        }
    ?>
</div>