<div class="users form">
    <div class="imgcontainer">
        <img src="/delivery-control/img/user.png" alt="Avatar" class="avatar">
    </div>
<?= $this->Flash->render('auth') ?>
    <?= $this->Form->create() ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Please enter your username and password') ?></h3>
        <?= $this->Form->input('username') ?>
        <?= $this->Form->input('password') ?>
    </div>
    <div class="form-group">
        <?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-log-in')).' '.__('Login'),['class' => 'btn btn-success']); ?>
        <?= $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-user')).' '.__('Add User'), ['controller' => 'users', 'action' => 'add'], ['class' => 'btn btn-default', 'escape'=>false]); ?>
    </div>
    <?= $this->Form->end() ?>
</div>
