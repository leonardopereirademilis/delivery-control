    <?= $this->Form->create($user) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Add User') ?></h3>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            echo $this->Form->input('email');
//            echo $this->Form->input('addresses._ids', ['options' => $addresses]);
            if(isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin'])) {
                echo $this->Form->input('roles._ids', ['options' => $roles]);
            }else{
                echo $this->Form->hidden('roles._ids', ['value' => 2, 'id' => 'roles-ids']);
            }
        ?>
    </div>
    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>