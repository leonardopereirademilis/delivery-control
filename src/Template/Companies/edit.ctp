    <?= $this->Form->create($company) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Edit Company') ?></h3>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('resource_id', ['options' => $resources]);
            echo $this->Form->input('position', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
        ?>
    </div>

    <?php echo $this->element('/Maps/mapSearch'); ?>

    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>