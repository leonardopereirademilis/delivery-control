<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= h($company->name) ?></h3>
    <div class="table-responsive"><table class="table table-striped table-bordered">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($company->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Resource') ?></th>
            <td><?= $company->has('resource') ? $this->Html->link($company->resource->id, ['controller' => 'Resources', 'action' => 'view', $company->resource->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($company->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($company->address->address) ?></td>
        </tr>
    </table></div>

    <?php echo $this->element('/Maps/mapSearch'); ?>

    <div class="related">
        <h4><?= __('Related Carriers') ?></h4>
        <?php if (!empty($company->carriers)): ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Base Position') ?></th>
                <th scope="col"><?= __('Position') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Datemodified') ?></th>
                <!-- <th scope="col"><?//= __('Company Id') ?></th> -->
                <th scope="col"><?= __('IMEI') ?></th>
                <!-- <th scope="col"><?//= __('Resource Id') ?></th> -->
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($company->carriers as $carriers): ?>
            <tr>
                <td><?= h($carriers->id) ?></td>
                <td><?= h($carriers->name) ?></td>
                <td><?= h($carriers->base_position) ?></td>
                <td><?= h($carriers->position) ?></td>
                <td><?= h($carriers->status) ?></td>
                <td><?= h($carriers->datemodified) ?></td>
                <!-- <td><?//= h($carriers->company_id) ?></td> -->
                <td><?= h($carriers->imei) ?></td>
                <!-- <td><?//= h($carriers->resource_id) ?></td> -->
                <td class="actions">
                    <?php
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['CarriersController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['controller' => 'Carriers', 'action' => 'view', $carriers->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['CarriersController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['controller' => 'Carriers', 'action' => 'edit', $carriers->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['CarriersController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['controller' => 'Carriers', 'action' => 'delete', $carriers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $carriers->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                        }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table></div>
        <?php else: ?>
            <div class="alert alert-info">No record found.</div>
        <?php endif; ?>
    </div>
</div>