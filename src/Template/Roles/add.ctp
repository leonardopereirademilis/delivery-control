    <?= $this->Form->create($role) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Add Role') ?></h3>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('permissions._ids', ['options' => $permissions]);
            echo $this->Form->input('users._ids', ['options' => $users]);
        ?>
    </div>
    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>