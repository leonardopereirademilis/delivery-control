<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Permissions Roles') ?></h3>
    <?php if (count($permissionsRoles) > 0) { ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Permissions.caption') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('role_id') ?></th>
                    <th scope="col"><?= __('Self') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($permissionsRoles as $permissionsRole): ?>
                <tr>
                    <td><?= $this->Number->format($permissionsRole->id) ?></td>
                    <td><?= $permissionsRole->has('permission') ? $this->Html->link($permissionsRole->permission->caption, ['controller' => 'Permissions', 'action' => 'view', $permissionsRole->permission->id]) : '' ?></td>
                    <td><?= $permissionsRole->has('role') ? $this->Html->link($permissionsRole->role->name, ['controller' => 'Roles', 'action' => 'view', $permissionsRole->role->id]) : '' ?></td>
                    <td><?= h($permissionsRole->self) ?></td>
                    <td class="actions">
                        <?php
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['PermissionsRolesController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['action' => 'view', $permissionsRole->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['PermissionsRolesController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['action' => 'edit', $permissionsRole->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['PermissionsRolesController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['action' => 'delete', $permissionsRole->id], ['confirm' => __('Are you sure you want to delete # {0}?', $permissionsRole->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                            }
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table></div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    <?php } else { ?>
        <div class="alert alert-info">
            No permissions roles found.
        </div>
    <?php }?>
</div>