<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= h($permissionsRole->id) ?></h3>
    <div class="table-responsive"><table class="table table-striped table-bordered">
        <tr>
            <th scope="row"><?= __('Permission') ?></th>
            <td><?= $permissionsRole->has('permission') ? $this->Html->link($permissionsRole->permission->id, ['controller' => 'Permissions', 'action' => 'view', $permissionsRole->permission->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= $permissionsRole->has('role') ? $this->Html->link($permissionsRole->role->name, ['controller' => 'Roles', 'action' => 'view', $permissionsRole->role->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($permissionsRole->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Self') ?></th>
            <td><?= $this->Text->autoParagraph(h($permissionsRole->self)); ?></td>
        </tr>
    </table></div>
</div>