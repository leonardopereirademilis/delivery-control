    <?= $this->Form->create($permissionsRole) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Edit Permissions Role') ?></h3>
        <?php
            echo $this->Form->input('permission_id', ['options' => $permissions]);
            echo $this->Form->input('role_id', ['options' => $roles]);
            echo $this->Form->input('self');
        ?>
    </div>
    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>