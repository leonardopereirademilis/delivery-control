<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('/Menu/menu'); ?>
</nav>
<div class="orders form large-9 medium-8 columns content">
    <?= $this->Form->create($order) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Edit Order') ?></h3>
        <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('order_status_id', ['options' => $orderStatus, 'empty' => true]);
        ?>
    </div>
    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>
</div>
