<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= h($plan->id) ?></h3>
    <div class="table-responsive"><table class="table table-striped table-bordered">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $plan->has('user') ? $this->Html->link($plan->user->name, ['controller' => 'Users', 'action' => 'view', $plan->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($plan->id) ?></td>
        </tr>
    </table></div>
    <div class="related">
        <h4><?= __('Related Resources') ?></h4>
        <?php if (!empty($plan->resources)): ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Resource') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Plan Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($plan->resources as $resources): ?>
            <tr>
                <td><?= h($resources->id) ?></td>
                <td><?= h($resources->resource) ?></td>
                <td><?= h($resources->type) ?></td>
                <td><?= h($resources->plan_id) ?></td>
                <td class="actions">
                    <?php
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['ResourcesController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['controller' => 'Resources', 'action' => 'view', $resources->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['ResourcesController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['controller' => 'Resources', 'action' => 'edit', $resources->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['ResourcesController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['controller' => 'Resources', 'action' => 'delete', $resources->id], ['confirm' => __('Are you sure you want to delete # {0}?', $resources->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                        }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table></div>
        <?php else: ?>
            <div class="alert alert-info">No record found.</div>
        <?php endif; ?>
    </div>
</div>