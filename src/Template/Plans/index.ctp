<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Plans') ?></h3>
    <?php if (count($plans) > 0) { ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($plans as $plan): ?>
                <tr>
                    <td><?= $this->Number->format($plan->id) ?></td>
                    <td><?= $plan->has('user') ? $this->Html->link($plan->user->name, ['controller' => 'Users', 'action' => 'view', $plan->user->id]) : '' ?></td>
                    <td class="actions">
                        <?php
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['PlansController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['action' => 'view', $plan->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['PlansController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['action' => 'edit', $plan->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['PlansController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['action' => 'delete', $plan->id], ['confirm' => __('Are you sure you want to delete # {0}?', $plan->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                            }
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table></div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    <?php } else { ?>
        <div class="alert alert-info">
            No plans found.
        </div>
    <?php }?>
</div>