    <?= $this->Form->create($permission) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Add Permission') ?></h3>
        <?php
            echo $this->Form->input('controller');
            echo $this->Form->input('action');
            echo $this->Form->input('caption');
            echo $this->Form->input('menuitem');
            echo $this->Form->input('roles._ids', ['options' => $roles]);
        ?>
    </div>
    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>