    <?= $this->Form->create($carrier) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Edit Carrier') ?></h3>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('base_position');
            echo $this->Form->input('position');
            echo $this->Form->input('status', ['options' => ['Offline' => 'Offline','Online' => 'Online']]);
            echo $this->Form->input('datemodified');
            echo $this->Form->input('company_id', ['options' => $companies]);
            echo $this->Form->input('imei');
            echo $this->Form->input('resource_id', ['options' => $resources]);
        ?>
    </div>
    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>
