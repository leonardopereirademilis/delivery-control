<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Carriers') ?></h3>
    <?php if (count($carriers) > 0) { ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('base_position') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('position') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('datemodified') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('company_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('imei') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('resource_id') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($carriers as $carrier): ?>
                <tr>
                    <td><?= $this->Number->format($carrier->id) ?></td>
                    <td><?= h($carrier->name) ?></td>
                    <td><?= h($carrier->base_position) ?></td>
                    <td><?= h($carrier->position) ?></td>
                    <td><?= h($carrier->datemodified) ?></td>
                    <td><?= $carrier->has('company') ? $this->Html->link($carrier->company->name, ['controller' => 'Companies', 'action' => 'view', $carrier->company->id]) : '' ?></td>
                    <td><?= h($carrier->imei) ?></td>
                    <td><?= $carrier->has('resource') ? $this->Html->link($carrier->resource->id, ['controller' => 'Resources', 'action' => 'view', $carrier->resource->id]) : '' ?></td>
                    <td class="actions">
                        <?php
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['CarriersController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['action' => 'view', $carrier->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['CarriersController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['action' => 'edit', $carrier->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['CarriersController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['action' => 'delete', $carrier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $carrier->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                            }
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table></div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    <?php } else { ?>
        <div class="alert alert-info">
            No carriers found.
        </div>
    <?php }?>
</div>