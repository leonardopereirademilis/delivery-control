<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= h($carrier->name) ?></h3>
    <div class="table-responsive"><table class="table table-striped table-bordered">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($carrier->name) ?></td>
        </tr>
        <?php
            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['CarriersController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                ?>
                <tr>
                    <th scope="row"><?= __('Base Position') ?></th>
                    <td><?= h($carrier->base_position) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Position') ?></th>
                    <td><?= h($carrier->position) ?></td>
                </tr>
                <?php
            }
        ?>
        <tr>
            <th scope="row"><?= __('Company') ?></th>
            <td><?= $carrier->has('company') ? $this->Html->link($carrier->company->name, ['controller' => 'Companies', 'action' => 'view', $carrier->company->id]) : '' ?></td>
        </tr>
        <?php
            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['CarriersController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                ?>
                <tr>
                    <th scope="row"><?= __('IMEI') ?></th>
                    <td><?= h($carrier->imei) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Resource') ?></th>
                    <td><?= $carrier->has('resource') ? $this->Html->link($carrier->resource->id, ['controller' => 'Resources', 'action' => 'view', $carrier->resource->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($carrier->id) ?></td>
                </tr>
                <?php
            }
        ?>
        <tr>
            <th scope="row"><?= __('Datemodified') ?></th>
            <td><?= h($carrier->datemodified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Text->autoParagraph(h($carrier->status)); ?></td>
        </tr>
    </table></div>

    <div class="related">
        <h4><?= __('Related Dispatchs') ?></h4>
        <?php if (!empty($carrier->dispatchs)): ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User') ?></th>
                <?php
                    if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                        ?>
                            <th scope="col"><?= __('Destination') ?></th>
                        <?php
                    }
                ?>
                <th scope="col"><?= __('Carrier') ?></th>
                <th scope="col"><?= __('Timestart') ?></th>
                <th scope="col"><?= __('Timeend') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <?php
                    //if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                        ?>
                            <!-- <th scope="col"><?//= __('Token') ?></th> -->
                        <?php
                    //}
                ?>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($carrier->dispatchs as $dispatchs): ?>
            <tr>
                <td><?= h($dispatchs->id) ?></td>
                <td><?= $this->Html->link(h($dispatchs->user->name), ['controller' => 'Users', 'action' => 'view', $dispatchs->user->id]); ?></td>
                <?php
                    if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                        ?>
                            <td><?= h($dispatchs->destination) ?></td>
                        <?php
                    }
                ?>
                <td><?= $this->Html->link(h($carrier->name), ['controller' => 'Carriers', 'action' => 'view', $carrier->id]); ?></td>
                <td><?= h($dispatchs->timestart) ?></td>
                <td><?= h($dispatchs->timeend) ?></td>
                <td><?= h($dispatchs->status) ?></td>
                <?php
                    //if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                        ?>
                            <!-- <td><?//= h($dispatchs->token) ?></td> -->
                        <?php
                    //}
                ?>
                <td class="actions">
                    <?php
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['controller' => 'Dispatchs', 'action' => 'view', $dispatchs->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['controller' => 'Dispatchs', 'action' => 'edit', $dispatchs->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['controller' => 'Dispatchs', 'action' => 'delete', $dispatchs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dispatchs->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                        }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table></div>
        <?php else: ?>
            <div class="alert alert-info">No record found.</div>
        <?php endif; ?>
    </div>

    <?php echo $this->element('/Maps/mapView'); ?>
</div>