    <?= $this->Form->create($addressesUser) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Edit Addresses User') ?></h3>
        <?php
            echo $this->Form->input('address_id', ['options' => $addresses]);
            echo $this->Form->input('user_id', ['options' => $users]);
        ?>
    </div>
    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>