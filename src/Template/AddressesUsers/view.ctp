<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= h($addressesUser->id) ?></h3>
    <div class="table-responsive"><table class="table table-striped table-bordered">
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= $addressesUser->has('address') ? $this->Html->link($addressesUser->address->name, ['controller' => 'Addresses', 'action' => 'view', $addressesUser->address->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $addressesUser->has('user') ? $this->Html->link($addressesUser->user->name, ['controller' => 'Users', 'action' => 'view', $addressesUser->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($addressesUser->id) ?></td>
        </tr>
    </table></div>
</div>