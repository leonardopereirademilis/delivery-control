<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Delivery Control';
?>
<!DOCTYPE html>
<html lang="pt-br" ng-app="myApp">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('custom.css') ?>

    <!-- load angular via CDN -->
    <script src="//code.angularjs.org/1.3.0-rc.1/angular.min.js"></script>
    <?= $this->Html->script('app') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Latest compiled JavaScript -->
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Datepicker -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/locale/pt-br.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

</head>
<body>


    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


                <a class="navbar-brand" href="/delivery-control">
                    Delivery Control
<!--                    <img src="/delivery-control/img/delivery-control-small.png" alt="Logo" class="logo">-->
                </a>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php echo $this->element('/Menu/menuTop'); ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php
                        if (isset($this->request->Session()->read('Auth')['User']['id'])) {
                            // user is logged in, show logout..user menu etc
                            echo '<li>' . $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-user')).' '.$this->request->Session()->read('Auth')['User']['name'], ['controller' => 'Users', 'action' => 'view', $this->request->Session()->read('Auth')['User']['id']], ['escape'=>false]) . '</li>';
                            echo '<li>' . $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-log-out')).' '.__('Log Out'), ['controller' => 'Users', 'action' => 'logout'], ['escape'=>false]) . '</li>';
                        } else {
                            // the user is not logged in
                            echo '<li>' . $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-log-in')).' '.__('Log in'), ['controller' => 'Users', 'action' => 'login'], ['escape'=>false]) . '</li>';
                        }
                    ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="jumbotron text-center">
        <h1>Delivery Control</h1>
    </div>

    <div class="container">
        <div class="row content">
            <div class="col-sm-12 main-content">
                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>
            </div>
        </div>
    </div>

    <footer>
        <div class="container bg-grey">
            <h2 class="text-center">CONTATO</h2>
            <div class="row">
                <div class="col-sm-5">
                    <p><span class="glyphicon glyphicon-map-marker"></span> Florianópolis-SC, Brasil</p>
                    <p><span class="glyphicon glyphicon-phone"></span> +55 (48)99934-9570</p>
                    <p><span class="glyphicon glyphicon-envelope"></span> contato@lpdemilis.com</p>
                </div>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <input class="form-control" id="name" name="name" placeholder="Nome" type="text" required>
                        </div>
                        <div class="col-sm-6 form-group">
                            <input class="form-control" id="email" name="email" placeholder="E-mail" type="email" required>
                        </div>
                    </div>
                    <textarea class="form-control" id="comments" name="comments" placeholder="Mensagem" rows="5"></textarea><br>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <button class="btn btn-success pull-right" type="submit">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>
