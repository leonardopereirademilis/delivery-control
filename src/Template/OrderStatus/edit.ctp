<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('/Menu/menu'); ?>
</nav>
<div class="orderStatus form large-9 medium-8 columns content">
    <?= $this->Form->create($orderStatus) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Edit Order Status') ?></h3>
        <?php
            echo $this->Form->input('status');
        ?>
    </div>
    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>
</div>
