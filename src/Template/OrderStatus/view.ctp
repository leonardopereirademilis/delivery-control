<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <?php echo $this->element('/Menu/menu'); ?>
</nav>
<div class="orderStatus view large-9 medium-8 columns content">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= h($orderStatus->id) ?></h3>
    <div class="table-responsive"><table class="table table-striped table-bordered">
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($orderStatus->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($orderStatus->id) ?></td>
        </tr>
    </table></div>
    <div class="related">
        <h4><?= __('Related Orders') ?></h4>
        <?php if (!empty($orderStatus->orders)): ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Order Status Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($orderStatus->orders as $orders): ?>
            <tr>
                <td><?= h($orders->id) ?></td>
                <td><?= h($orders->user_id) ?></td>
                <td><?= h($orders->order_status_id) ?></td>
                <td class="actions">
                    <?php
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['OrdersController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->id]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['OrdersController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['controller' => 'Orders', 'action' => 'edit', $orders->id]);
                        }
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['OrdersController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                            echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->id)]);
                        }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table></div>
        <?php endif; ?>
    </div>
</div>
