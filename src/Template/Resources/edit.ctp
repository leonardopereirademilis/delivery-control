    <?= $this->Form->create($resource) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Edit Resource') ?></h3>
        <?php
            echo $this->Form->input('resource', ['options' => ['Company' => 'Company','Carrier' => 'Carrier']]);
            echo $this->Form->input('type', ['options' => ['Free' => 'Free','Paid' => 'Paid']]);
            echo $this->Form->input('plan_id', ['options' => $plans]);
        ?>
    </div>
    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>