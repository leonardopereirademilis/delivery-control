<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Resources') ?></h3>
    <?php if (count($resources) > 0) { ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('plan_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('resource') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                    <th scope="col"><?= __('Related resource') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resources as $resource): ?>
                <tr>
                    <td><?= $this->Number->format($resource->id) ?></td>
                    <td><?= $resource->has('plan') ? $this->Html->link($resource->plan->id, ['controller' => 'Plans', 'action' => 'view', $resource->plan->id]) : '' ?></td>
                    <td><?= h($resource->resource) ?></td>
                    <td><?= h($resource->type) ?></td>
                    <td>
                        <?php
                            if($resource->resource === 'Company') {
                                if(isset($resource->companies[0]['name'])){
                                    echo $this->Html->link($resource->companies[0]['name'], ['controller' => 'Companies', 'action' => 'view', $resource->companies[0]['id']]);
                                }
                            }else{
                                if(isset($resource->carriers[0]['name'])){
                                    echo $this->Html->link($resource->carriers[0]['name'], ['controller' => 'Carriers', 'action' => 'view', $resource->carriers[0]['id']]);
                                }
                            }
                        ?>
                    </td>
                    <td class="actions">
                        <?php
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['ResourcesController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['action' => 'view', $resource->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['ResourcesController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['action' => 'edit', $resource->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['ResourcesController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['action' => 'delete', $resource->id], ['confirm' => __('Are you sure you want to delete # {0}?', $resource->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                            }
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table></div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    <?php } else { ?>
        <div class="alert alert-info">
            No resources found.
        </div>
    <?php }?>
</div>