    <?= $this->Form->create($resource) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Add Resource') ?></h3>
        <?php
            echo $this->Form->input('resource', ['options' => ['Company' => 'Company','Carrier' => 'Carrier']]);
            echo $this->Form->input('type', ['options' => ['Free' => 'Free','Paid' => 'Paid']]);
            echo $this->Form->input('plan_id', ['options' => $plans]);
        ?>
    </div>
    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>

    <div>
        <div class="text-center">
            <h2>Pricing</h2>
            <h4>Choose a payment plan that works for you</h4>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">
                        <h1>Plano 1</h1>
                    </div>
                    <div class="panel-body">
                        <p>Quantidade de carregadores: <strong>1 (um)</strong></p>
                        <p>Quantidade de empresas: <strong>1 (uma)</strong></p>
                        <p>Valor da parcela (R$): <strong>10,00</strong></p>
                        <p>Frequência das cobranças: <strong>mensal</strong></p>
                        <p>Duração das adesões: <strong>sem expiração</strong></p>
                    </div>
                    <div class="panel-footer">
                        <h3>R$ 10,00</h3>
                        <h4>por mês</h4>

                        <form action="/delivery-control/orders/checkout" method="post" class="form-sign-up" target="_blank">
                            <input type="submit" name="submit" class="btn btn-lg" value="Contratar">
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">
                        <h1>Plano 5</h1>
                    </div>
                    <div class="panel-body">
                        <p>Quantidade de carregadores: <strong>5 (cinco)</strong></p>
                        <p>Quantidade de empresas: <strong>1 (uma)</strong></p>
                        <p>Valor da parcela (R$): <strong>50,00</strong></p>
                        <p>Frequência das cobranças: <strong>mensal</strong></p>
                        <p>Duração das adesões: <strong>sem expiração</strong></p>
                    </div>
                    <div class="panel-footer">
                        <h3>R$ 50,00</h3>
                        <h4>por mês</h4>

                        <form action="/delivery-control/orders/checkout" method="post" class="form-sign-up" target="_blank">
                            <input type="submit" name="submit" class="btn btn-lg" value="Contratar">
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">
                        <h1>Plano 10</h1>
                    </div>
                    <div class="panel-body">
                        <p>Quantidade de carregadores: <strong>10 (dez)</strong></p>
                        <p>Quantidade de empresas: <strong>1 (uma)</strong></p>
                        <p>Valor da parcela (R$): <strong>100,00</strong></p>
                        <p>Frequência das cobranças: <strong>mensal</strong></p>
                        <p>Duração das adesões: <strong>sem expiração</strong></p>
                    </div>
                    <div class="panel-footer">
                        <h3>R$ 100,00</h3>
                        <h4>por mês</h4>

                        <form action="/delivery-control/orders/checkout" method="post" class="form-sign-up" target="_blank">
                            <input type="submit" name="submit" class="btn btn-lg" value="Contratar">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>