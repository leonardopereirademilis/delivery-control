<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Roles Users') ?></h3>
    <?php if (count($rolesUsers) > 0) { ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('role_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rolesUsers as $rolesUser): ?>
                <tr>
                    <td><?= $this->Number->format($rolesUser->id) ?></td>
                    <td><?= $rolesUser->has('role') ? $this->Html->link($rolesUser->role->name, ['controller' => 'Roles', 'action' => 'view', $rolesUser->role->id]) : '' ?></td>
                    <td><?= $rolesUser->has('user') ? $this->Html->link($rolesUser->user->name, ['controller' => 'Users', 'action' => 'view', $rolesUser->user->id]) : '' ?></td>
                    <td class="actions">
                        <?php
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['RolesUsersController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['action' => 'view', $rolesUser->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['RolesUsersController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['action' => 'edit', $rolesUser->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['RolesUsersController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['action' => 'delete', $rolesUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $rolesUser->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                            }
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table></div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    <?php } else { ?>
        <div class="alert alert-info">
            No users roles found.
        </div>
    <?php }?>
</div>