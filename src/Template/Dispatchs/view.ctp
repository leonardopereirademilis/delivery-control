<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= h($dispatch->id) ?></h3>
    <div class="table-responsive"><table class="table table-striped table-bordered">
        <?php
            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                ?>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($dispatch->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Token') ?></th>
                    <td><?= h($dispatch->token) ?></td>
                </tr>
                <?php
            }
        ?>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $dispatch->has('user') ? $this->Html->link($dispatch->user->name, ['controller' => 'Users', 'action' => 'view', $dispatch->user->id]) : '' ?></td>
        </tr>
        <?php
            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                ?>
                    <tr>
                        <th scope="row"><?= __('Address') ?></th>
                        <td><?= h($dispatch->address->address) ?></td>
                    </tr>
                <?php
            }
        ?>
        <tr>
            <th scope="row"><?= __('Carrier') ?></th>
            <td><?= $dispatch->has('carrier') ? $this->Html->link($dispatch->carrier->name, ['controller' => 'Carriers', 'action' => 'view', $dispatch->carrier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Timestart') ?></th>
            <td><?= h($dispatch->timestart) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Timeend') ?></th>
            <td><?= h($dispatch->timeend) ?></td>
        </tr>
        <?php
        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
            ?>
                <tr>
                    <th scope="row"><?= __('URL') ?></th>
                    <td><?= $this->Html->link('/dispatchs/view/'.h($dispatch->id).'?token='.h($dispatch->token), '/dispatchs/view/'.h($dispatch->id).'?token='.h($dispatch->token)) ?></td>
                </tr>
            <?php
        }
        ?>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Text->autoParagraph(h($dispatch->status)); ?></td>
        </tr>
    </table></div>

    <?php echo $this->element('/Maps/mapView'); ?>
</div>
