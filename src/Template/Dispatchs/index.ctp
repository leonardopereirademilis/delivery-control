<?php
    $option1 = 'selected';
    $option2 = '';
    $option3 = '';

    if (isset($this->request->data['status'])) {
        switch ($this->request->data['status']) {
            case 'Delivered':
                $option1 = '';
                $option2 = 'selected';
                $option3 = '';
                break;
            case 'Canceled':
                $option1 = '';
                $option2 = '';
                $option3 = 'selected';
                break;
            default:
                $option1 = 'selected';
                $option2 = '';
                $option3 = '';
                break;
        }
    }
?>
<form class="form-inline" action="#" method="post">
    <div class="form-group">
        <label for="Status">Status:</label>
        <select class="form-control" id="status" name="status">
            <option <?= $option1 ?>>Pending</option>
            <option <?= $option2 ?>>Delivered</option>
            <option <?= $option3 ?>>Canceled</option>
        </select>

        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-filter"></span> Buscar</button>
    </div>
</form>

<br>

<div class="content-view">
    <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __('Dispatchs') ?></h3>

    <?php if (count($dispatchs) > 0) { ?>
        <div class="table-responsive"><table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                    <?php
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                            ?>
                            <th scope="col"><?= __('Address') ?></th>
                            <?php
                        }
                    ?>
                    <th scope="col"><?= $this->Paginator->sort('carrier_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('timestart') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('timeend') ?></th>
                    <?php
                        //if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                            ?>
                            <!-- <th scope="col"><?//= $this->Paginator->sort('token') ?></th> -->
                            <?php
                        //}
                    ?>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dispatchs as $dispatch): ?>
                <tr>
                    <td><?= $this->Number->format($dispatch->id) ?></td>
                    <td><?= $dispatch->has('user') ? $this->Html->link($dispatch->user->name, ['controller' => 'Users', 'action' => 'view', $dispatch->user->id]) : '' ?></td>
                    <?php
                        if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                            ?>
                            <td><?= h($dispatch->address->address) ?></td>
                            <?php
                        }
                    ?>
                    <td><?= $dispatch->has('carrier') ? $this->Html->link($dispatch->carrier->name, ['controller' => 'Carriers', 'action' => 'view', $dispatch->carrier->id]) : '' ?></td>
                    <td><?= h($dispatch->status) ?></td>
                    <td><?= h($dispatch->timestart) ?></td>
                    <td><?= h($dispatch->timeend) ?></td>
                    <?php
                        //if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))) {
                            ?>
                            <!-- <td><?//= h($dispatch->token) ?></td> -->
                            <?php
                        //}
                    ?>
                    <td class="actions">
                        <?php
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['view'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-search')).' '.__('View'), ['action' => 'view', $dispatch->id], ['class' => 'btn btn-success btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['edit'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-pencil')).' '.__('Edit'), ['action' => 'edit', $dispatch->id], ['class' => 'btn btn-primary btn-md', 'escape'=>false]);
                            }
                            if((isset($this->request->Session()->read('Auth')['User']['allpermissions']['DispatchsController']['delete'])) || (isset($this->request->Session()->read('Auth')['User']['allpermissions']['admin']))){
                                echo $this->Form->postLink($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash')).' '.__('Delete'), ['action' => 'delete', $dispatch->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dispatch->id), 'class' => 'btn btn-danger btn-md', 'escape'=>false]);
                            }
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table></div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
        </div>
    <?php } else { ?>
        <div class="alert alert-info">
            No dispatch found.
        </div>
    <?php }?>
</div>