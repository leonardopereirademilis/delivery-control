<?= $this->Form->create($dispatch) ?>
<div class="form-group" ng-controller="dispatchsController">
    <h3>
        <span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Add Dispatch') ?>
    </h3>

    <div ng-show="addDispatch.parte1Show">
        <?php
        echo $this->Form->input('user_search', ['type' => 'text']);
        echo $this->Form->input('user_name', ['readonly', 'placeholder' => __('Select an user'), 'title' => __('Select an user'), 'ng-model' => 'addDispatch.user_name', 'ng-change' => 'userSelected()']);
        echo $this->Form->hidden('user_id', ['id' => 'user-id', 'ng-model' => 'addDispatch.user_id']);
        ?>
    </div>

    <div ng-show="addDispatch.parte2Show">
        <?php
        echo $this->Form->input('destination', ['readonly', 'placeholder' => __('Select an address or add a new one'), 'title' => __('Select an address or add a new one')]);
        echo $this->Form->hidden('address_id', ['id' => 'address-id']);
        ?>

        <div class="table-responsive" id="addressesTable">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Action</th>
                    <th>Selected</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-show="addDispatch.addressShow">
                    <td colspan="4">
                        No Addresses available
                    </td>
                </tr>
                <tr ng-repeat="a in addDispatch.addresses">
                    <td>{{ a.name }}</td>
                    <td>{{ a.address }}</td>
                    <td>
                        <button type="button" class="btn btn-success btn-md"
                                ng-click="addDispatch.selectAddress(a.lat, a.lng, a.id)"><span
                                    class="glyphicon glyphicon-map-marker"></span> Select Address
                        </button>
                    </td>
                    <td>
                        <div ng-attr-id="{{ 'selected-' + a.id }}"></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <?php
        echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-plus')).' '.__('New Address'), ['action' => 'add', $dispatch->id, 'controller' => 'Addresses'], ['class' => 'btn btn-success btn-md', 'escape'=>false, 'id' => 'newAddressButton']);
        ?>

    </div>

    <div ng-show="addDispatch.parte3Show">
        <?php
        echo $this->Form->input('company_id', ['options' => $companies, 'empty' => array(0 => __('Select company')), 'ng-model' => 'addDispatch.company', 'ng-change' => 'companyChange()']);
        echo $this->Form->input('carrier_id', ['ng-options' => 'carrier as carrier.name for carrier in addDispatch.carriers track by carrier.id', 'ng-model' => 'addDispatch.carrier', 'ng-disabled' => 'addDispatch.disableCarrier', 'ng-change' => 'carrierChange()']);
        ?>
    </div>

    <div ng-show="addDispatch.parte4Show">
        <?php
        echo $this->Form->input('timestart');

        //            echo '<div class="input-group date" id="datetimepicker2">
        //                    <input type="text" class="form-control" id="timestart"/>
        //                    <span class="input-group-addon">
        //                        <span class="glyphicon glyphicon-calendar"></span>
        //                    </span>
        //                  </div>';

        echo $this->Form->input('status', ['options' => ['Pending' => 'Pending' /*,'Delivered' => 'Delivered','Canceled' => 'Canceled'*/]]);
        ?>
    </div>
</div>

<?php //echo $this->element('/Maps/mapSearch'); ?>

<div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')) . ' ' . __('Submit'), ['class' => 'btn btn-success', 'id' => 'dispatchAddSubmit', 'disabled']) ?></div>
<?= $this->Form->end() ?>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <h3>
                        <span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Add Address') ?>
                    </h3>
                </h4>
            </div>
            <div class="modal-body">
                <iframe id="newAddressFrame"></iframe>
            </div>
            <!--                                <div class="modal-footer">-->
            <!--                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
            <!--                                </div>-->
        </div>
    </div>
</div>


<script>
    $('#user-search').autocomplete({
        source: '<?= $this->Url->build(['controller' => 'Users', 'action' => 'search', "_ext" => "json"]); ?>',
        minLength: 3,
        keypress: function (event, ui) {

        },
        select: function (event, ui) {
            $("#user-search").val(ui.item.label);
            $("#user-id").val(ui.item.value);
            $("#user-name").val(ui.item.label + " - id: " + ui.item.value);
            angular.element(document.getElementById('user-name')).triggerHandler('change');
            $("#newAddressButton").attr("href", "/delivery-control/addresses/add?userid=" + ui.item.value + "&username=" + ui.item.label + "&return=dispatchs");
            var url = "/delivery-control/addresses/add?userid=" + ui.item.value + "&username=" + ui.item.label.trim() + "&noheader=true";
            $("#newAddressButton").removeClass('disabled');
            $("#newAddressFrame").attr("src", url);

            return false;
        }
    });

    $('#user-search').on('keypress', function (e) {
        $("#user-id").val(undefined);
    });

    //        $(function () {
    //            $('#datetimepicker2').datetimepicker({
    //                locale: 'pt-br'
    //            });
    //        });

    $('#myModal').on('show.bs.modal', function () {
        $('.modal-content').css('height', $(window).height() * 0.9);
        $('#newAddressFrame').css('height', ($(window).height() * 0.9) - 120);
    });

</script>