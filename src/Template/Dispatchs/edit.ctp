    <script>
        $(document).ready(function(){
            checkStatus();

            $( "#status" ).change(function() {
                checkStatus();
            });
        });

        function checkStatus(){
            if ($("#status").val() == "Pending"){
                $('[name="timeend[year]"]').prop('disabled', true);
                $('[name="timeend[month]"]').prop('disabled', true);
                $('[name="timeend[day]"]').prop('disabled', true);
                $('[name="timeend[hour]"]').prop('disabled', true);
                $('[name="timeend[minute]"]').prop('disabled', true);

                $('[name="timeend[year]"]').addClass('disabled');
                $('[name="timeend[month]"]').addClass('disabled');
                $('[name="timeend[day]"]').addClass('disabled');
                $('[name="timeend[hour]"]').addClass('disabled');
                $('[name="timeend[minute]"]').addClass('disabled');

            }else{
                $('[name="timeend[year]"]').prop('disabled', false);
                $('[name="timeend[month]"]').prop('disabled', false);
                $('[name="timeend[day]"]').prop('disabled', false);
                $('[name="timeend[hour]"]').prop('disabled', false);
                $('[name="timeend[minute]"]').prop('disabled', false);

                $('[name="timeend[year]"]').removeClass('disabled');
                $('[name="timeend[month]"]').removeClass('disabled');
                $('[name="timeend[day]"]').removeClass('disabled');
                $('[name="timeend[hour]"]').removeClass('disabled');
                $('[name="timeend[minute]"]').removeClass('disabled');
            }
        }

    </script>

    <?= $this->Form->create($dispatch) ?>
    <div class="form-group">
        <h3><span class="glyphicon glyphicon-<?= strtolower($this->request->controller) ?>"></span> <?= __(' Edit Dispatch') ?></h3>
        <?php
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => array(0 => __('Select user'))]);
            echo $this->Form->input('carrier_id', ['options' => $carriers, 'empty' => array(0 => __('Select carrier'))]);
            echo $this->Form->input('timestart');
            echo $this->Form->input('timeend');
            echo $this->Form->input('status', ['options' => ['Pending' => 'Pending','Delivered' => 'Delivered','Canceled' => 'Canceled']]);
//            echo $this->Form->input('token');
            echo $this->Form->input('destination', ['readonly', 'placeholder' => __('Select a place in the map'), 'title' => __('Select a place in the map')]);
        ?>
    </div>

    <?php echo $this->element('/Maps/mapSearch'); ?>

    <div class="form-group"><?= $this->Form->button($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-ok')).' '.__('Submit'),['class' => 'btn btn-success']) ?></div>
    <?= $this->Form->end() ?>