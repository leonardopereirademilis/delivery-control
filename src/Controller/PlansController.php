<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Resource;

/**
 * Plans Controller
 *
 * @property \App\Model\Table\PlansTable $Plans
 */
class PlansController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if($this->isAdmin($this->Auth->user())){
            $this->paginate = [
                'contain' => ['Users']
            ];
        }else{
            $this->paginate = [
                'contain' => ['Users'],
                'conditions' => ['Plans.user_id' => $this->Auth->user()['id']]
            ];
        }

        $plans = $this->paginate($this->Plans);

        $this->set(compact('plans'));
        $this->set('_serialize', ['plans']);
    }

    /**
     * View method
     *
     * @param string|null $id Plan id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $plan = $this->Plans->get($id, [
            'contain' => ['Users', 'Resources']
        ]);

        $this->set('plan', $plan);
        $this->set('_serialize', ['plan']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Configs');
        $configs = $this->Configs->find('all', [
            'conditions' => ["Configs.name in('freecompanies', 'freecarriers')"]
        ]);

        $freecompanies = 0;
        $freecarriers = 0;

        foreach ($configs as $config){
            if($config['name'] == 'freecompanies'){
                $freecompanies = $config['value'];
            }

            if($config['name'] == 'freecarriers'){
                $freecarriers = $config['value'];
            }
        }

        $plan = $this->Plans->newEntity();
        if ($this->request->is('post')) {
            $plan = $this->Plans->patchEntity($plan, $this->request->data);
            if ($this->Plans->save($plan)) {
                $this->Flash->success(__('The plan has been saved.'));

                $this->loadModel('Resources');

                for($i = 0; $i < $freecompanies; $i++){
                    $resource = new Resource();
                    $resource->resource = 'Company';
                    $resource->type = 'Free';
                    $resource->plan_id = $plan->id;
                    $this->Resources->save($resource);
                }

                for($i = 0; $i < $freecarriers; $i++){
                    $resource = new Resource();
                    $resource->resource = 'Carrier';
                    $resource->type = 'Free';
                    $resource->plan_id = $plan->id;
                    $this->Resources->save($resource);
                }

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The plan could not be saved. Please, try again.'));
            }
        }
        $users = $this->Plans->Users->find('list', ['limit' => 200]);
        $this->set(compact('plan', 'users'));
        $this->set('_serialize', ['plan']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Plan id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $plan = $this->Plans->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $plan = $this->Plans->patchEntity($plan, $this->request->data);
            if ($this->Plans->save($plan)) {
                $this->Flash->success(__('The plan has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The plan could not be saved. Please, try again.'));
            }
        }
        $users = $this->Plans->Users->find('list', ['limit' => 200]);
        $this->set(compact('plan', 'users'));
        $this->set('_serialize', ['plan']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Plan id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $plan = $this->Plans->get($id);
        if ($this->Plans->delete($plan)) {
            $this->Flash->success(__('The plan has been deleted.'));
        } else {
            $this->Flash->error(__('The plan could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
