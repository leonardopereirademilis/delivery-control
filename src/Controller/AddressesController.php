<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Addresses Controller
 *
 * @property \App\Model\Table\AddressesTable $Addresses
 */
class AddressesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if ((isset($this->request->params["_ext"])) && ($this->request->params["_ext"] == "json")) {
            $this->loadModel('Users');
            $users = $this->Users->find('all', [
                'contain' => ['Addresses'],
                'conditions' => ['Users.id' => $this->request->query["user"]]
            ]);
            $addresses = $users->first()->addresses;
        } else {
            $addresses = $this->paginate($this->Addresses);
        }

        $this->set(compact('addresses'));
        $this->set('_serialize', ['addresses']);
    }

    /**
     * View method
     *
     * @param string|null $id Address id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $address = $this->Addresses->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('address', $address);
        $this->set('_serialize', ['address']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $address = $this->Addresses->newEntity();
        if ($this->request->is('post')) {
            $address = $this->Addresses->patchEntity($address, $this->request->data);
            if ($this->Addresses->save($address)) {
                if(isset($address->userid)) {
                    $this->loadModel('AddressesUsers');
                    $addressesUsers = $this->AddressesUsers->newEntity();
                    $addressesUsers->address_id = $address->id;
                    $addressesUsers->user_id = $address->userid;
                    $this->AddressesUsers->save($addressesUsers);
                }

                $this->Flash->success(__('The address has been saved.'));
                if($address->return == "dispatchs"){
                    return $this->redirect(['controller' => 'Dispatchs','action' => 'add']);
                }else{
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->Flash->error(__('The address could not be saved. Please, try again.'));
            }
        }
        $users = $this->Addresses->Users->find('list', ['limit' => 200]);
        $this->set(compact('address', 'users'));
        $this->set('_serialize', ['address']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Address id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $address = $this->Addresses->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $address = $this->Addresses->patchEntity($address, $this->request->data);
            if ($this->Addresses->save($address)) {
                $this->Flash->success(__('The address has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The address could not be saved. Please, try again.'));
            }
        }
        $users = $this->Addresses->Users->find('list', ['limit' => 200]);
        $this->set(compact('address', 'users'));
        $this->set('_serialize', ['address']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Address id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $address = $this->Addresses->get($id);
        if ($this->Addresses->delete($address)) {
            $this->Flash->success(__('The address has been deleted.'));
        } else {
            $this->Flash->error(__('The address could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * @param $user
     * @return bool
     */
//    public function isAuthorized($user)
//    {
//        if(!parent::isAuthorized($user)){
//            $this->Flash->error(__('You are not authorized to access that location'));
//            return false;
//        }
//
//        if(!$this->isAdmin($user) && (!in_array($this->request->action, ['view', 'add']))){
//            if ($this->Auth->user()['id'] == $this->request->params['pass'][0]){
//                return true;
//            }
//
//            $this->Flash->error(__('You are not authorized to access that location'));
//            return false;
//        }
//
//        return true;
//    }
}
