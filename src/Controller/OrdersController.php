<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'OrderStatus']
        ];
        $orders = $this->paginate($this->Orders);

        $this->set(compact('orders'));
        $this->set('_serialize', ['orders']);
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
	    $order = $this->Orders->get($id, [
            'contain' => ['Users', 'OrderStatus']
        ]);

        require_once("Component/pagseguro/PagSeguro.class.php");

        $PagSeguro = new \PagSeguro();
        $P = $PagSeguro->getStatusByReference($id);
        echo $PagSeguro->getStatusText($P->status);
        echo "<pre>"; 
        print_r( $P->status);
        echo "</pre>";

        die();
//        $order->order_status_id = $P->status;
//
//        $this->Orders->save($order);

        $this->set('order', $order);
        $this->set('_serialize', ['order']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $users = $this->Orders->Users->find('list', ['limit' => 200]);
        $orderStatus = $this->Orders->OrderStatus->find('list', ['limit' => 200]);
        $this->set(compact('order', 'users', 'orderStatus'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $users = $this->Orders->Users->find('list', ['limit' => 200]);
        $orderStatus = $this->Orders->OrderStatus->find('list', ['limit' => 200]);
        $this->set(compact('order', 'users', 'orderStatus'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function checkout(){
        $order = $this->Orders->newEntity();
        $order->user = $this->Auth->user();

        if($this->Orders->save($order)){
            $url = "http://lpdemilis.com/pagseguro/checkout.php";
            $url .= "?codigo=" . $order->id;
            $url .= "&valor=100.0";
            $url .= "&descricao=VENDA DE TESTE";
            $url .= "&nome=Comprador Teste";
            $url .= "&email=lpdemilis@gmail.com";
            $url .= "&telefone=(48) 9934-9570";
            $url .= "&rua=Salvatina Feliciana dos Santos";
            $url .= "&numero=155";
            $url .= "&bairro=Itacorubi";
            $url .= "&cidade=Florianopolis";
            $url .= "&estado=SC";
            $url .= "&cep=88.034-600";
            $url .= "&codigo_pagseguro=";

            $this->redirect($url);
        }else {
            $this->Flash->error(__('The order could not be saved. Please, try again.'));

            $this->redirect(['controller' => 'resources', 'action' => 'add']);
        }
    }

    /**
     * @param Event $event
     * @return \Cake\Http\Response|null|void
     */
    public function beforeFilter(Event $event)
    {
        if(isset($this->Auth->user()['id'])){
            $this->Auth->allow(['checkout']);
        }
    }
}
