<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Addresses', 'Roles', 'Dispatchs', 'Plans', 'RolesUsers']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->loadModel('RolesUsers');
                $rolesUser = $this->RolesUsers->newEntity();
                $rolesUser->user_id = $user->id;
                $rolesUser->role_id = 2; //user role

                $this->RolesUsers->save($rolesUser);

                $this->Flash->success(__('The user has been saved.'));

                if (isset($this->Auth->user()['id'])){
                    return $this->redirect(['action' => 'index']);
                }else{
                    return $this->redirect(['action' => 'login']);
                }
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $addresses = $this->Users->Addresses->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'addresses', 'roles'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Addresses', 'Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $addresses = $this->Users->Addresses->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'addresses', 'roles'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * @param Event $event
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Permitir aos usuários se registrarem e efetuar logout.
        // Você não deve adicionar a ação de "login" a lista de permissões.
        // Isto pode causar problemas com o funcionamento normal do AuthComponent.
        if((isset($this->Auth->user()['id'])) && ($this->isAdmin($this->Auth->user()))){
            $this->Auth->allow(['logout', 'search']);
        }else{
            $this->Auth->allow(['add', 'logout', 'search']);
        }
    }

    /**
     * @return \Cake\Network\Response|null
     */
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {

                $user = $this->Users->get($user['id'], [
                    'contain' => ['Roles','Roles.Permissions']
                ]);

                $allpermissions = [];
                foreach ($user['roles'] as $role){
                    foreach ($role->permissions as $permission){
                        $allpermissions[$permission['controller']][$permission['action']] = $permission['caption'];
                    }
                }

                if($this->isAdmin($user)){
                    $allpermissions['admin'] = true;
                }

                $user->allpermissions = $allpermissions;

                $this->Auth->setUser($user);

                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Usuário ou senha ínvalido, tente novamente'));
        }
    }

    /**
     * @return \Cake\Network\Response|null
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * @param $user
     * @return bool
     */
    public function isAuthorized($user)
    {
        if(!parent::isAuthorized($user)){
            $this->Flash->error(__('You are not authorized to access that location'));
            return false;
        }

        if(!$this->isAdmin($user) && (!in_array($this->request->action, ['view', 'add']))){
            if ($this->Auth->user()['id'] == $this->request->params['pass'][0]){
                return true;
            }

            $this->Flash->error(__('You are not authorized to access that location'));
            return false;
        }

        return true;
    }

    /**
     *
     */
    public function search()
    {
        //if ($this->requrest->is('ajax')) {
            $this->autoRender = false;
            $name = $this->request->query['term'];
            $results = $this->Users->find('all', [
                'conditions' => [
                    'OR' => [
                        ['name LIKE' => '%' . $name . '%'],
                        ['email LIKE' => '%' . $name . '%']
                    ]
                ],
                'limit'=>20
            ]);
            $resultsArr = [];
            foreach ($results as $result) {
                $resultsArr[] =['label' => $result['name'] . ' (' . $result['email'] . ')', 'value' => $result['id']];
            }
            echo json_encode($resultsArr);
        //}
    }
}
