<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Carriers Controller
 *
 * @property \App\Model\Table\CarriersTable $Carriers
 */
class CarriersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if ((isset($this->request->params["_ext"])) && ($this->request->params["_ext"] == "json") && (isset($this->request->query['company']))) {
            $company_id = $this->request->query['company'];
            $carriers = $this->Carriers->find('list', [
                'conditions' => ['company_id' => $company_id]
            ]);
        } else {
            if ($this->isAdmin($this->Auth->user())) {
                $this->paginate = [
                    'contain' => ['Companies', 'Resources']
                ];
            } else {
                $this->paginate = [
                    'contain' => ['Companies', 'Resources', 'Resources.Plans'],
                    'conditions' => ['Plans.user_id' => $this->Auth->user()['id']]
                ];
            }

            $carriers = $this->paginate($this->Carriers);
        }

        $this->set(compact('carriers'));
        $this->set('_serialize', ['carriers']);
    }

    /**
     * View method
     *
     * @param string|null $id Carrier id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $carrier = $this->Carriers->get($id, [
            'contain' => ['Companies', 'Resources', 'Dispatchs', 'Dispatchs.Users']
        ]);

        $this->set('carrier', $carrier);
        $this->set('_serialize', ['carrier']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $carrier = $this->Carriers->newEntity();
        if ($this->request->is('post')) {
            $carrier = $this->Carriers->patchEntity($carrier, $this->request->data);
            if ($this->Carriers->save($carrier)) {
                $this->Flash->success(__('The carrier has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The carrier could not be saved. Please, try again.'));
            }
        }

        $resources_ids = [];
        $resources = $this->getAvailableCarriersResources();

        foreach ($resources as $resource) {
            array_push($resources_ids, $resource['id']);
        }

        $resources_ids = implode(',', $resources_ids);

        if (!$this->isAdmin($this->Auth->user())) {
            $resources = $this->Carriers->Resources->find('list', ['limit' => 200])
                ->where('id in (' . $resources_ids . ')');

            $companies = $this->Carriers->Companies->find('list', [
                'limit' => 200,
                'contain' => ['Resources', 'Resources.Plans'],
                'conditions' => ['Plans.user_id' => $this->Auth->user()['id']]]);
        } else {
            $resources = $this->Carriers->Resources->find('list', ['limit' => 200]);
            $companies = $this->Carriers->Companies->find('list', ['limit' => 200]);
        }

        $this->set(compact('carrier', 'companies', 'resources'));
        $this->set('_serialize', ['carrier']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Carrier id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $carrier = $this->Carriers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $carrier = $this->Carriers->patchEntity($carrier, $this->request->data);
            if ($this->Carriers->save($carrier)) {
                $this->Flash->success(__('The carrier has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The carrier could not be saved. Please, try again.'));
            }
        }
        $companies = $this->Carriers->Companies->find('list', ['limit' => 200]);
        $resources = $this->Carriers->Resources->find('list', ['limit' => 200]);
        $this->set(compact('carrier', 'companies', 'resources'));
        $this->set('_serialize', ['carrier']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Carrier id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $carrier = $this->Carriers->get($id);
        if ($this->Carriers->delete($carrier)) {
            $this->Flash->success(__('The carrier has been deleted.'));
        } else {
            $this->Flash->error(__('The carrier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * @param Event $event
     */
    public function beforeRender(Event $event)
    {
        if (in_array($this->request->action, ['add']) && !$this->isAdmin($this->Auth->user())) {
            $availableCarriersResources = $this->getAvailableCarriersResources();

            if (count($availableCarriersResources) == 0) {
                $this->Flash->success(__('You do not have resources.'));

                return $this->redirect(['controller' => 'Resources', 'action' => 'add']);
            }
        }
    }

    /**
     * @return array
     */
    public function getAvailableCarriersResources()
    {
        $this->loadModel('Resources');

        $resources = $this->Resources->find('all', [
            'contain' => ['Plans', 'Carriers'],
            'conditions' => ['Plans.user_id' => $this->Auth->user()['id'], 'Resources.resource' => 'Carrier'],
            'joins' => ['table' => 'carriers',
                'alias' => 'Carriers',
                'type' => 'LEFT',
                'conditions' => ['Carriers.resource_id = Resources.id'],
                'having' => 'Carriers.id IS NULL']
        ]);

        $resources = $resources->toArray();

        foreach ($resources as $key => $resource) {
            if (isset($resource->carriers[0]->id)) {
                unset($resources[$key]);
            }
        }

        $resources = array_values($resources);

        return $resources;
    }

    /**
     * action para view mensagem
     * aqui não definiremos nenhum corpo pois
     * não se faz necessário
     */
    public function mensagem()
    {
    }

    /**
     * função para a chamada ajax
     * funcionamento muito simples
     * setamos uma string para uma
     * variável chamada 'mensagem'
     * que ficará disponível
     * na em ajax_msg.ctp
     */
    public function ajaxMsg($id = null)
    {
        $this->viewBuilder()->layout("ajax");

        $carrier = $this->Carriers->get($id, [
            'contain' => ['Companies', 'Resources', 'Dispatchs']
        ]);

        $this->loadModel('Companies');
        $company = $this->Companies->get($carrier->company_id);
        $carrier->company = $company;

        $this->set('carrier', $carrier);
        $this->set('_serialize', ['carrier']);
    }

    /**
     * @param Event $event
     */

    public function beforeFilter(Event $event)
    {
        if (isset($this->request->params["_ext"]) && $this->request->params["_ext"] == "json") {
            $this->Auth->allow(['updateCarrier', 'getMyDispatchs']);
        }

        $this->Auth->allow(['mensagem', 'ajaxMsg']);

//        if(!isset($this->Auth->user()['id'])){
//            if(isset($this->request->query["imei"])){
//                $carrier = $this->Carriers->get($this->request->params['pass'][0]);
//
//                if($this->request->query["imei"] == $carrier->imei){
//                    $this->Auth->allow(['view']);
//                }else{
//                    $this->Flash->error(__('Invalid imei'));
//                }
//            }
//        }
    }

    /**
     * @param $user
     * @return bool
     */
    public function isAuthorized($user)
    {
        if (!parent::isAuthorized($user)) {
            $this->Flash->error(__('You are not authorized to access that location'));
            return false;
        }

        if (!$this->isAdmin($user) && (!in_array($this->request->action, ['index', 'add', 'delete']))) {
            $carriers = $this->Carriers->find('all', [
                'contain' => ['Resources.Plans'],
                'conditions' => ['Carriers.id' => $this->request->params['pass'][0], 'Plans.user_id' => $this->Auth->user()['id']]
            ]);

            if ($carriers->count() > 0) {
                return true;
            }

            $this->loadModel('Dispatchs');
            $dispatchs = $this->Dispatchs->find('all', [
                'conditions' => ['Dispatchs.carrier_id' => $this->request->params['pass'][0], 'Dispatchs.user_id' => $this->Auth->user()['id'], 'Dispatchs.status' => 'Pending']
            ]);

            if ($dispatchs->count() > 0) {
                return true;
            }

            $this->Flash->error(__('You are not authorized to access that location'));
            return false;
        }

        return true;
    }

    /**
     * @return null
     */
    public function updateCarrier()
    {
        if ($this->request->params["_ext"] == "json") {

            $data = $this->request->data;

            if (!isset($this->request->query["imei"])) {
                $this->set("status", "ERROR");
                $this->set("message", "Required param imei");
                $this->set("content", $data);
                $this->set("_serialize", array("status", "message", "content"));
                return null;
            }

            if (!isset($this->request->query["position"])) {
                $this->set("status", "ERROR");
                $this->set("message", "Required param position");
                $this->set("content", $data);
                $this->set("_serialize", array("status", "message", "content"));
                return null;
            }

            if (!isset($this->request->query["status"])) {
                $this->set("status", "ERROR");
                $this->set("message", "Required param status");
                $this->set("content", $data);
                $this->set("_serialize", array("status", "message", "content"));
                return null;
            }

            $carriers = $this->Carriers->find('all', [
                'conditions' => ['Carriers.imei' => $this->request->query["imei"]]
            ]);

            if ($carriers->count() == 0) {
                $this->set("status", "ERROR");
                $this->set("message", "IMEI not found");
                $this->set("content", $data);
                $this->set("_serialize", array("status", "message", "content"));
                return null;
            }

            foreach ($carriers as $carrier) {
                $carrier->position = $this->request->query["position"];
                $carrier->status = $this->request->query["status"];
                $carrier->datemodified = time();
                $this->Carriers->save($carrier);

                $carrier->company = $this->Carriers->Companies->get($carrier->company_id);
                $carrier->resource = $this->Carriers->Resources->get($carrier->resource_id);
            }
        }

        $this->set("status", "OK");
        $this->set(compact('carriers', 'companies', 'resources'));
        $this->set('_serialize', ['status', 'carriers']);
    }

    /**
     *
     */
    public function getMyDispatchs()
    {
        $data = $this->request->data;

        if (!isset($this->request->query["imei"])) {
            $this->set("status", "ERROR");
            $this->set("message", "Required param imei");
            $this->set("content", $data);
            $this->set("_serialize", array("status", "message", "content"));
            return null;
        }

        $carriers = $this->Carriers->find('all', [
            'conditions' => ['Carriers.imei' => $this->request->query["imei"]]
        ]);

        if ($carriers->count() == 0) {
            $this->set("status", "ERROR");
            $this->set("message", "IMEI not found");
            $this->set("content", $data);
            $this->set("_serialize", array("status", "message", "content"));
            return null;
        }

        foreach ($carriers as $carrier) {
            $dispatchsPending = $this->Carriers->Dispatchs->find('all', [
                'contain' => ['Users','Addresses'],
                'conditions' => ['Dispatchs.carrier_id' => $carrier->id, 'Dispatchs.status' => 'Pending'],
+                'order' => ['Dispatchs.timestart' => 'ASC'],
+                'limit' => 20
            ]);

            $carrier->dispatchsPending = $dispatchsPending->jsonSerialize();

            $dispatchsDelivered = $this->Carriers->Dispatchs->find('all', [
                'contain' => ['Users','Addresses'],
                'conditions' => ['Dispatchs.carrier_id' => $carrier->id, 'Dispatchs.status' => 'Delivered'],
                'order' => ['Dispatchs.timeend' => 'DESC'],
                'limit' => 20
            ]);

            $carrier->dispatchsDelivered = $dispatchsDelivered->jsonSerialize();
        }

        $this->set(compact('carriers'));
        $this->set('_serialize', ['carriers']);
    }

}
