<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Dispatchs Controller
 *
 * @property \App\Model\Table\DispatchsTable $Dispatchs
 */
class DispatchsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if (isset($this->request->data['status'])){
            $status = $this->request->data['status'];
        }else{
            $status = 'Pending';
        }

        if($this->isAdmin($this->Auth->user())){
            $this->paginate = [
                'contain' => ['Users', 'Carriers', 'Addresses']
            ];
        }else{
            $this->paginate = [
                'contain' => ['Users', 'Carriers', 'Carriers.Companies.Resources.Plans', 'Addresses'],
                'conditions' => [
                    'OR' => [
                        ['Dispatchs.user_id' => $this->Auth->user()['id']],
                        ['Plans.user_id' => $this->Auth->user()['id']]
                    ],
                    'Dispatchs.status' => $status
                ]
            ];
        }

        $dispatchs = $this->paginate($this->Dispatchs);

        $this->set(compact('dispatchs'));
        $this->set('_serialize', ['dispatchs']);
    }

    /**
     * View method
     *
     * @param string|null $id Dispatch id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dispatch = $this->Dispatchs->get($id, [
            'contain' => ['Users', 'Carriers', 'Addresses']
        ]);

        $this->loadModel('Companies');
        $company = $this->Companies->get($dispatch->carrier->company_id);
        $dispatch->carrier->company = $company;

        $dispatchs = $this->Dispatchs->find('all', [
            'conditions' => ['Dispatchs.carrier_id' => $dispatch->carrier_id, 'Dispatchs.status' => 'Pending']
        ]);

        $dispatch->dispatchs = $dispatchs->toArray();

        $this->set('dispatch', $dispatch);
        $this->set('_serialize', ['dispatch']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dispatch = $this->Dispatchs->newEntity();
        if ($this->request->is('post')) {
            $dispatch = $this->Dispatchs->patchEntity($dispatch, $this->request->data);
            $dispatch->token = md5(uniqid(rand(), true));
            if ($this->Dispatchs->save($dispatch)) {
                $this->Flash->success(__('The dispatch has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dispatch could not be saved. Please, try again.'));
            }
        }
        $users = $this->Dispatchs->Users->find('list', ['limit' => 200]);
        if($this->isAdmin($this->Auth->user())){
            $carriers = $this->Dispatchs->Carriers->find('list', ['limit' => 200]);
            $companies = $this->Dispatchs->Carriers->Companies->find('list', ['limit' => 200]);
        }else {
            $carriers = $this->Dispatchs->Carriers->find('list', [
                'limit' => 200,
                'contain' => ['Resources.Plans'],
                'conditions' => ['Plans.user_id' => $this->Auth->user()['id']]
            ]);

            $companies = $this->Dispatchs->Carriers->Companies->find('list', [
                'limit' => 200,
                'contain' => ['Resources.Plans'],
                'conditions' => ['Plans.user_id' => $this->Auth->user()['id']]
            ]);
        }
        $this->set(compact('dispatch', 'users', 'carriers', 'companies'));
        $this->set('_serialize', ['dispatch']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dispatch id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dispatch = $this->Dispatchs->get($id, [
            'contain' => ['Carriers']
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dispatch = $this->Dispatchs->patchEntity($dispatch, $this->request->data);
            if ($this->Dispatchs->save($dispatch)) {
                $this->Flash->success(__('The dispatch has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dispatch could not be saved. Please, try again.'));
            }
        }
        $users = $this->Dispatchs->Users->find('list', ['limit' => 200]);
        $carriers = $this->Dispatchs->Carriers->find('list', [
            'limit' => 200,
            'conditions' => ['company_id' => $dispatch->carrier->company_id]
        ]);
        $this->set(compact('dispatch', 'users', 'carriers'));
        $this->set('_serialize', ['dispatch']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dispatch id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dispatch = $this->Dispatchs->get($id);
        if ($this->Dispatchs->delete($dispatch)) {
            $this->Flash->success(__('The dispatch has been deleted.'));
        } else {
            $this->Flash->error(__('The dispatch could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
    action para view mensagem
    aqui não definiremos nenhum corpo pois
    não se faz necessário
     */
    public function mensagem(){}
    /**
    função para a chamada ajax
    funcionamento muito simples
    setamos uma string para uma
    variável chamada 'mensagem'
    que ficará disponível
    na em ajax_msg.ctp
     */
    public function ajaxMsg($id = null){
        $this->viewBuilder()->layout("ajax");

        $dispatch = $this->Dispatchs->get($id, [
            'contain' => ['Users', 'Carriers']
        ]);

        $this->loadModel('Companies');
        $company = $this->Companies->get($dispatch->carrier->company_id);
        $dispatch->carrier->company = $company;

        $dispatchs = $this->Dispatchs->find('all', [
            'conditions' => ['Dispatchs.carrier_id' => $dispatch->carrier_id, 'Dispatchs.status' => 'Pending']
        ]);

        $dispatch->dispatchs = $dispatchs->toArray();

        $this->set('dispatch', $dispatch);
        $this->set('_serialize', ['dispatch']);
    }

    /**
     * @param Event $event
     */

    public function beforeFilter(Event $event)
    {
        if (isset($this->request->params["_ext"]) && $this->request->params["_ext"] == "json") {
            $this->Auth->allow(['updateDispatch']);
        }

        $this->Auth->allow(['mensagem', 'ajaxMsg']);

        if(!isset($this->Auth->user()['id'])){
            if(isset($this->request->query["token"])){
                $dispatch = $this->Dispatchs->get($this->request->params['pass'][0]);

                if($this->request->query["token"] == $dispatch->token){
                    $this->Auth->allow(['view']);
                }else{
                    $this->Flash->error(__('Invalid token'));
                }
            }
        }
    }

    /**
     * @param $user
     * @return bool
     */
    public function isAuthorized($user)
    {
        if(!parent::isAuthorized($user)){
            $this->Flash->error(__('You are not authorized to access that location'));
            return false;
        }

        if(!$this->isAdmin($user) && (!in_array($this->request->action, ['index', 'add', 'delete']))){
            $dispatchs = $this->Dispatchs->find('all', [
                'contain' => ['Carriers.Resources.Plans'],
                'conditions' => [
                    'OR' => [
                        ['Dispatchs.id' => $this->request->params['pass'][0], 'Dispatchs.user_id' => $this->Auth->user()['id'], 'Dispatchs.status' => 'Pending'],
                        ['Plans.user_id' => $this->Auth->user()['id']]
                    ]
                ]
            ]);

            if ($dispatchs->count() > 0) {
                return true;
            }

            $this->Flash->error(__('You are not authorized to access that location'));
            return false;
        }

        return true;
    }

    /**
     * @return null
     */
    public function updateDispatch()
    {
        if ($this->request->params["_ext"] == "json") {

            $data = $this->request->data;

            if (!isset($this->request->query["deliveryToken"])) {
                $this->set("status", "ERROR");
                $this->set("message", "Required param deliveryToken");
                $this->set("content", $data);
                $this->set("_serialize", array("status", "message", "content"));
                return null;
            }

            if (!isset($this->request->query["status"])) {
                $this->set("status", "ERROR");
                $this->set("message", "Required param status");
                $this->set("content", $data);
                $this->set("_serialize", array("status", "message", "content"));
                return null;
            }

            $status = array("Pending", "Delivered", "Canceled");
            if (!in_array($this->request->query["status"], $status)) {
                $this->set("status", "ERROR");
                $this->set("message", "Invalid param value (status)");
                $this->set("content", $data);
                $this->set("_serialize", array("status", "message", "content"));
                return null;
            }

            $dispatchs = $this->Dispatchs->find('all', [
                'conditions' => ['Dispatchs.token' => $this->request->query["deliveryToken"]]
            ]);

            if ($dispatchs->count() == 0) {
                $this->set("status", "ERROR");
                $this->set("message", "Dispatch not found");
                $this->set("content", $data);
                $this->set("_serialize", array("status", "message", "content"));
                return null;
            }

            foreach ($dispatchs as $dispatch) {
                $dispatch->status = $this->request->query["status"];

                if($dispatch->status == "Pending"){
                    $dispatch->timeend = null;
                }else{
                    $dispatch->timeend = time();
                }

                $this->Dispatchs->save($dispatch);
            }
        }

        $this->loadModel('Carriers');
        $carriers = $this->Carriers->find('all', [
            'conditions' => ['Carriers.id' => $dispatch->carrier_id]
        ]);

        if ($carriers->count() == 0) {
            $this->set("status", "ERROR");
            $this->set("message", "IMEI not found");
            $this->set("content", $data);
            $this->set("_serialize", array("status", "message", "content"));
            return null;
        }

        foreach ($carriers as $carrier) {
            $dispatchsPending = $this->Carriers->Dispatchs->find('all', [
                'contain' => ['Users'],
                'conditions' => ['Dispatchs.carrier_id' => $carrier->id, 'Dispatchs.status' => 'Pending'],
+                'order' => ['Dispatchs.timestart' => 'ASC'],
+                'limit' => 20
            ]);

            $carrier->dispatchsPending = $dispatchsPending->jsonSerialize();

            $dispatchsDelivered = $this->Carriers->Dispatchs->find('all', [
                'contain' => ['Users'],
                'conditions' => ['Dispatchs.carrier_id' => $carrier->id, 'Dispatchs.status' => 'Delivered'],
                'order' => ['Dispatchs.timeend' => 'DESC'],
                'limit' => 20
            ]);

            $carrier->dispatchsDelivered = $dispatchsDelivered->jsonSerialize();
        }

        $this->set(compact('carriers'));
        $this->set('_serialize', ['carriers']);
    }

    /**
     * @param Event $event
     */

    public function beforeRender(Event $event)
    {
        if (in_array($this->request->action, ['add']) && !$this->isAdmin($this->Auth->user())) {
            $availableCompaniesResources = $this->getAvailableCompaniesResources();

            if (count($availableCompaniesResources) == 0) {
                $this->Flash->success(__('You do not have resources.'));

                return $this->redirect(['controller' => 'Resources', 'action' => 'add']);
            }
        }
    }

    /**
     * @return array
     */
    public function getAvailableCompaniesResources(){
        $this->loadModel('Resources');

        $resources = $this->Resources->find('all', [
            'contain' => ['Plans', 'Companies'],
            'conditions' => ['Plans.user_id' => $this->Auth->user()['id'], 'Resources.resource' => 'Company'],
            'joins' => ['table' => 'companies',
                'alias' => 'Companies',
                'type' => 'LEFT',
                'conditions' => ['Companies.resource_id = Resources.id'],
                'having' => 'Companies.id IS NULL']
        ]);

        $resources = $resources->toArray();

        foreach ($resources as $key => $resource){
            if (isset($resource->companies[0]->id)){
                unset($resources[$key]);
            }
        }

        $resources = array_values($resources);

        return $resources;
    }
}
