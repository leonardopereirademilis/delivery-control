<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OrderStatus Controller
 *
 * @property \App\Model\Table\OrderStatusTable $OrderStatus
 */
class OrderStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $orderStatus = $this->paginate($this->OrderStatus);

        $this->set(compact('orderStatus'));
        $this->set('_serialize', ['orderStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Order Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $orderStatus = $this->OrderStatus->get($id, [
            'contain' => ['Orders']
        ]);

        $this->set('orderStatus', $orderStatus);
        $this->set('_serialize', ['orderStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $orderStatus = $this->OrderStatus->newEntity();
        if ($this->request->is('post')) {
            $orderStatus = $this->OrderStatus->patchEntity($orderStatus, $this->request->getData());
            if ($this->OrderStatus->save($orderStatus)) {
                $this->Flash->success(__('The order status has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order status could not be saved. Please, try again.'));
        }
        $this->set(compact('orderStatus'));
        $this->set('_serialize', ['orderStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Order Status id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $orderStatus = $this->OrderStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $orderStatus = $this->OrderStatus->patchEntity($orderStatus, $this->request->getData());
            if ($this->OrderStatus->save($orderStatus)) {
                $this->Flash->success(__('The order status has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order status could not be saved. Please, try again.'));
        }
        $this->set(compact('orderStatus'));
        $this->set('_serialize', ['orderStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $orderStatus = $this->OrderStatus->get($id);
        if ($this->OrderStatus->delete($orderStatus)) {
            $this->Flash->success(__('The order status has been deleted.'));
        } else {
            $this->Flash->error(__('The order status could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
