<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Companies Controller
 *
 * @property \App\Model\Table\CompaniesTable $Companies
 */
class CompaniesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if($this->isAdmin($this->Auth->user())){
            $this->paginate = [
                'contain' => ['Resources', 'Addresses']
            ];
        }else{
            $this->paginate = [
                'contain' => ['Resources', 'Resources.Plans', 'Addresses'],
                'conditions' => ['Plans.user_id' => $this->Auth->user()['id']]
            ];
        }

        $companies = $this->paginate($this->Companies);

        $this->set(compact('companies'));
        $this->set('_serialize', ['companies']);
    }

    /**
     * View method
     *
     * @param string|null $id Company id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => ['Resources', 'Carriers', 'Addresses']
        ]);

        $this->set('company', $company);
        $this->set('_serialize', ['company']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Addresses');
        $address = $this->Addresses->newEntity();
        $company = $this->Companies->newEntity();
        if ($this->request->is('post')) {
            $address = $this->Addresses->patchEntity($address, $this->request->data);

            if ($this->Addresses->save($address)) {
                $company = $this->Companies->patchEntity($company, $this->request->data);
                $company->address_id = $address->id;

                if ($this->Companies->save($company)) {
                    $this->Flash->success(__('The company has been saved.'));

                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Addresses->delete($address);
                    $this->Flash->error(__('The company could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->error(__('The company could not be saved. Please, try again.'));
            }


        }

        $resources_ids = [];
        $resources = $this->getAvailableCompaniesResources();

        foreach ($resources as $resource){
            array_push($resources_ids, $resource['id']);
        }

        $resources_ids = implode(',', $resources_ids);

        if(!$this->isAdmin($this->Auth->user())){
            $resources = $this->Companies->Resources->find('list', ['limit' => 200])
                ->where('id in ('.$resources_ids.')');
        }else{
            $resources = $this->Companies->Resources->find('list', ['limit' => 200]);
        }

        $this->set(compact('company', 'resources'));
        $this->set('_serialize', ['company']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Company id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $company = $this->Companies->patchEntity($company, $this->request->data);
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('The company has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The company could not be saved. Please, try again.'));
            }
        }

        $resources_ids = [];
        $resources = $this->getAvailableCompaniesResources();

        foreach ($resources as $resource){
            array_push($resources_ids, $resource['id']);
        }

        array_push($resources_ids, $company->resource_id);

        $resources_ids = implode(',', $resources_ids);

        if(!$this->isAdmin($this->Auth->user())){
            $resources = $this->Companies->Resources->find('list', ['limit' => 200])
                ->where('id in ('.$resources_ids.')');
        }else{
            $resources = $this->Companies->Resources->find('list', ['limit' => 200]);
        }

        $this->set(compact('company', 'resources'));
        $this->set('_serialize', ['company']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Company id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $company = $this->Companies->get($id);
        if ($this->Companies->delete($company)) {
            $this->Flash->success(__('The company has been deleted.'));
        } else {
            $this->Flash->error(__('The company could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * @param Event $event
     */

    public function beforeRender(Event $event)
    {
        if (in_array($this->request->action, ['add']) && !$this->isAdmin($this->Auth->user())) {
            $availableCompaniesResources = $this->getAvailableCompaniesResources();

            if (count($availableCompaniesResources) == 0) {
                $this->Flash->success(__('You do not have resources.'));

                return $this->redirect(['controller' => 'Resources', 'action' => 'add']);
            }
        }
    }

    /**
     * @return array
     */
    public function getAvailableCompaniesResources(){
        $this->loadModel('Resources');

        $resources = $this->Resources->find('all', [
            'contain' => ['Plans', 'Companies'],
            'conditions' => ['Plans.user_id' => $this->Auth->user()['id'], 'Resources.resource' => 'Company'],
            'joins' => ['table' => 'companies',
                        'alias' => 'Companies',
                        'type' => 'LEFT',
                        'conditions' => ['Companies.resource_id = Resources.id'],
                        'having' => 'Companies.id IS NULL']
        ]);

        $resources = $resources->toArray();

        foreach ($resources as $key => $resource){
            if (isset($resource->companies[0]->id)){
                unset($resources[$key]);
            }
        }

        $resources = array_values($resources);

        return $resources;
    }

    /**
     * @param $user
     * @return bool
     */
    public function isAuthorized($user)
    {
        if(!parent::isAuthorized($user)){
            $this->Flash->error(__('You are not authorized to access that location'));
            return false;
        }

        if(!$this->isAdmin($user) && (!in_array($this->request->action, ['index', 'add', 'delete']))){
            $companies = $this->Companies->find('all', [
                'contain' => ['Resources.Plans'],
                'conditions' => ['Companies.id' => $this->request->params['pass'][0], 'Plans.user_id' => $this->Auth->user()['id']]
            ]);

            if ($companies->count() > 0) {
                return true;
            }

            $this->loadModel('Dispatchs');
            $dispatchs = $this->Dispatchs->find('all', [
                'contain' => ['Carriers.Companies'],
                'conditions' => ['Companies.id' => $this->request->params['pass'][0], 'Dispatchs.user_id' => $this->Auth->user()['id'], 'Dispatchs.status' => 'Pending']
            ]);
            
            if ($dispatchs->count() > 0) {
                return true;
            }

            $this->Flash->error(__('You are not authorized to access that location'));
            return false;
        }

        return true;
    }
}
