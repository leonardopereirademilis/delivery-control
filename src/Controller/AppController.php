<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'loginRedirect' => [
                'controller' => 'Dispatchs',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Pages',
                'action' => 'display',
                'home'
            ]
        ]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * @param Event $event
     */

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['display']);
    }

    /**
     * @param $user
     * @return bool
     */
    public function isAuthorized($user)
    {
        // Admin pode acessar todas as actions
        if ($this->isAdmin($user)) {
            $menu = $this->getMenu();
            $this->set('menu', $menu);
            $this->set('_serialize', ['permissions']);
            return true;
        }

        foreach ($user['roles'] as $role){
            foreach ($role->permissions as $permission){
                if(($permission->controller === ($this->request->controller."Controller")) && ($permission->action === $this->request->action)){
                    if($permission->_joinData->self === 'Y'){
                        if((!isset($this->Auth->user()['id'])) || ($this->Auth->user()['id'] != $user->id)){
                            $this->Flash->error(__('You are not authorized to access that location'));
                            return false;
                        }

                    }
                    
                    $menu = $this->getMenu();
                    $this->set('menu', $menu);
                    $this->set('_serialize', ['permissions']);
                    return true;
                }
            }
        }

        $this->Flash->error(__('You are not authorized to access that location'));

        // Bloqueia acesso por padrão
        return false;
    }

    /**
     * @param $user
     * @return bool
     */
    public function isAdmin($user)
    {
        $this->loadModel('Users');
        $user = $this->Users->get($user['id'], [
            'contain' => ['Roles']
        ]);

        foreach ($user->roles as $user_role){
            if (isset($user_role['name']) && $user_role['name'] === 'admin') {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getMenu(){
        $this->loadModel('Users');
        $user = $this->Users->get($this->Auth->user()['id'], [
            'contain' => ['Roles', 'Roles.Permissions']
        ]);

        $menu = [];

        if($this->isAdmin($this->Auth->user())){
            $this->loadModel('Permissions');
            $permissions = $this->Permissions->find('all');
            foreach ($permissions as $permission) {
                if ($permission->menuitem === 'Y') {
                    $menu[$permission->controller][$permission->action] = $permission->caption;
                }
            }
        }else {
            foreach ($user->roles as $role) {
                foreach ($role['permissions'] as $permission) {
                    if ($permission->menuitem === 'Y') {
                        $menu[$permission->controller][$permission->action] = $permission->caption;
                    }
                }
            }
        }

        foreach ($menu as $controller => $action){
            ksort($menu[$controller]);
        }

        //ksort($menu);

        return $menu;
    }
}
